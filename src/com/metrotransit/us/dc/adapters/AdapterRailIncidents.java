package com.metrotransit.us.dc.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.metrotransit.R;
import com.metrotransit.us.dc.vo.Incident;

public class AdapterRailIncidents extends ArrayAdapter{

	private final Activity activity;
	private final List incidents;
	
	@SuppressWarnings("unchecked")
	public AdapterRailIncidents(Activity activity, List objects){
		super(activity, R.layout.list_incidents_row, objects);
		this.activity = activity;
		this.incidents = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View rowView = convertView;
		RailIncidentsRowView railIncidentsRowView = null;
		
		if(null == rowView){
			// Get a new instance of the row layout view
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.list_incidents_row, parent, false);
			
			// Hold the view objects in an object, so they don't need to be
			// re-fetched
			railIncidentsRowView = new RailIncidentsRowView();
			railIncidentsRowView.description = (TextView) rowView.findViewById(R.id.textViewIncidentDescription);
			railIncidentsRowView.listViewLinesAffected = (ListView) rowView.findViewById(R.id.listViewLinesAffected);
			/*railIncidentsRowView.line = (TextView) rowView.findViewById(R.id.textViewIncidentLine);*/
			
			// Cache the view objects in the tag, so they can be re-accessed
			// later
			rowView.setTag(railIncidentsRowView);
		}else{
			railIncidentsRowView = (RailIncidentsRowView) rowView.getTag();
		}
		
		// Transfer the Train data from the data object to the view objects
		Incident incident = (Incident) incidents.get(position);
		railIncidentsRowView.description.setText(incident.getDescription());
		
		List linesAffected = new ArrayList<String>();
		if(null != incident.getLinesAffected() && !incident.getLinesAffected().isEmpty()){
			Collections.addAll(linesAffected, incident.getLinesAffected().split(";"));
			AdapterLinesAffected adapterLinesAffected = new AdapterLinesAffected(
					activity, linesAffected);
			railIncidentsRowView.listViewLinesAffected.setAdapter(adapterLinesAffected);
		}
		// TODO Imp. lines affected will be list and not single line
		/*railIncidentsRowView.line.setText(incident.getLinesAffected());*/
		
		return rowView;
	}
	
	@Override
	public boolean isEnabled(int position){
		return false;
	}
	
	protected static class RailIncidentsRowView {
		protected TextView description;
		protected ListView listViewLinesAffected;
		/*protected TextView line;*/
	}
	
}
