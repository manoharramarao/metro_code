package com.metrotransit.us.dc.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Tell my genius friend {@link #onReceive(Context, Intent)} what needs to be done as soon as phone boots up.
 * 
 * @author manohar
 * 
 */
public class ReceiverBootComplete extends BroadcastReceiver{

	private static final String TAG = "ReceiverBootComplete";
	
	/**
	 * Tell me what needs to be done when phone boots up. As of now I call {@link ServiceBootComplete}
	 * @param _context
	 * @param _intent
	 */
	@Override
	public void onReceive(Context _context, Intent _intent) {
		
		Log.d(TAG, "onReceive");
		/*Toast.makeText(_context, "metro ReceiverBootComplete received", Toast.LENGTH_LONG).show();*/
		Intent intent = new Intent(_context.getApplicationContext(), ServiceBootComplete.class);
		_context.getApplicationContext().startService(intent);
	}
}
