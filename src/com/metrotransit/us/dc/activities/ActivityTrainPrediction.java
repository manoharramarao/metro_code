package com.metrotransit.us.dc.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.fragments.FragmentTrainPrediction;
import com.metrotransit.us.dc.fragments.FragmentTrainPrediction.TrainPrediction;

/**
 * I predict next upcoming trains for a given list of stations for US DC Metro. I am called on tapping next train on
 * home screen.
 * 
 * @author manohar
 * 
 */
public class ActivityTrainPrediction extends FragmentActivity implements TrainPrediction{

	private static final String TAG = "ActivityTrainPrediction";
	private FragmentTrainPrediction fragmentTrainPrediction;
	private String stationName;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.train_prediction);
		fragmentTrainPrediction = (FragmentTrainPrediction)getSupportFragmentManager().findFragmentById(R.id.fragment_train_prediction);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
	}

	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(TAG, "onOptionsItemSelected");

		switch (item.getItemId()) {
		case android.R.id.home:
			Log.d(TAG, "up navigation button clicked");
			Intent parentActivityIntent = new Intent(this, ActivityHome.class);
			parentActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(parentActivityIntent);
			finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onStationSelected(String stationName) {
		Log.d(TAG, "onStationSelected");
		this.stationName = stationName;
		fragmentTrainPrediction.updateStationInfo(stationName);
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState");
		outState.putString("stationName", stationName);
		if(null != fragmentTrainPrediction){
			getSupportFragmentManager().putFragment(outState,"fragmentTrainPrediction",fragmentTrainPrediction);
		}
		super.onSaveInstanceState(outState);
    }
	
	@Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
        		fragmentTrainPrediction = (FragmentTrainPrediction) getSupportFragmentManager().getFragment(savedInstanceState, "fragmentTrainPrediction");
        		fragmentTrainPrediction.updateStationInfo(savedInstanceState.getString("stationName"));
        		stationName = savedInstanceState.getString("stationName");
	}
}
