package com.metrotransit.us.dc.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.fragments.FragmentBusPrediction;
import com.metrotransit.us.dc.fragments.FragmentBusPrediction.BusPrediction;

/**
 * I am Called when next bus is tapped on home screen. I call wmata and predict next upcoming busses on given stop.
 * 
 * @author manohar
 * 
 */
public class ActivityBusPrediction extends FragmentActivity implements BusPrediction{

	private static final String TAG = "ActivityBusPrediction";
	private FragmentBusPrediction fragmentBusPrediction;
	private String stopName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bus_prediction);
		fragmentBusPrediction = (FragmentBusPrediction)getSupportFragmentManager().findFragmentById(R.id.fragment_bus_prediction);
		this.getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
	}

	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(TAG, "onOptionsItemSelected");

		switch (item.getItemId()) {
		case android.R.id.home:
			Log.d(TAG, "up navigation button clicked");
			Intent parentActivityIntent = new Intent(this, ActivityHome.class);
			parentActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(parentActivityIntent);
			finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onStopSelected(String stopName) {
		Log.d(TAG, "onStopSelected");
		this.stopName = stopName;
		fragmentBusPrediction.updateStopInfo(stopName);
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState");
		outState.putString("stationName", stopName);
		if(null != fragmentBusPrediction){
			getSupportFragmentManager().putFragment(outState,"fragmentBusPrediction",fragmentBusPrediction);
		}
		super.onSaveInstanceState(outState);
    }
	
	@Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
        fragmentBusPrediction = (FragmentBusPrediction) getSupportFragmentManager().getFragment(savedInstanceState, "fragmentBusPrediction");
        fragmentBusPrediction.updateStopInfo(savedInstanceState.getString("stopName"));
        stopName = savedInstanceState.getString("stopName");
	}
}
