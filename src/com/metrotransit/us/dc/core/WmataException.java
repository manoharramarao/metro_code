package com.metrotransit.us.dc.core;


public class WmataException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String additionalInfo = "";
	
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}
	
	/**
	 * @param string
	 */
	public WmataException(String string) {
		super(string);
	}
	
	/**
	 * Wrap an exception as a WmataException.
	 */
	WmataException(Exception e) {
		super(e);
		// avoid gratuitous nesting of exceptions
		assert !(e instanceof WmataException) : e;
	}
	
	public WmataException(String string, String additionalInfo) {
		this(string);
		this.setAdditionalInfo(additionalInfo);
	}
	
	/**
	 * Indicates a 404: resource does not exist error from WMATA.
	 * Note: Can be throw in relation to suspended users (e.g. spambots).
	 */
	public static class E404 extends WmataException {
		public E404(String string) {
			super(string);
		}
		private static final long serialVersionUID = 1L;		
	}
	
	/**
	 * Indicates a rate limit error (i.e. you've over-used WMATA)
	 * NOTE: Testing to be done for this. Not sure howmuch they have this.
	 */
	public static class RateLimit extends WmataException {
		public RateLimit(String string) {
			super(string);
		}
		private static final long serialVersionUID = 1L;		
	}

	/**
	 * Something has gone wrong. Swear I don't know what is it.
	 */
	public static class Unexplained extends WmataException {
		public Unexplained(String msg) {
			super(msg);
		}
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * A timeout exception. Could be wmata is overloaded.
	 */
	public static class Timeout extends WmataException {
		public Timeout(String string) {
			super(string);
		}
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * A code 50X error (e.g. 502) - indicating something went wrong at 
	 * WMATA's end. Usually retrying in a minute will fix this.
	 */
	public static class E50X extends WmataException {
		public E50X(String string) {
			super(string);
		}
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * A Forbidden exception. This is thrown if the authenticating used does not have
	 * the right to make a request. Usually happens if API key is wrong
	 */
	public static class E403 extends WmataException {
		public E403(String string) {
			super(string);
		}
		private static final long serialVersionUID = 1L;
	}
	
	/**
	 * An unauthorised exception. This is thrown (eg) if a password is wrong
	 * or a login is required.
	 * NOTE: This will not be used in our case as we do not need any authentication for
	 * any request
	 */
	public static class E401 extends WmataException {
		public E401(String string) {
			super(string);
		}
		private static final long serialVersionUID = 1L;
	}
}
