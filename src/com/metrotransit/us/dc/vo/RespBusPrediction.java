package com.metrotransit.us.dc.vo;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class RespBusPrediction {

	@SerializedName("Predictions")
	private List<BusInfo> predictions;
	
	@SerializedName("StopName")
	private String stopName;
	
	public List<BusInfo> getPredictions() {
		return predictions;
	}

	public void setPredictions(List<BusInfo> predictions) {
		this.predictions = predictions;
	}

	public String getStopName() {
		return stopName;
	}

	public void setStopName(String stopName) {
		this.stopName = stopName;
	}
	
}
