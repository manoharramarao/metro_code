package com.metrotransit.us.dc.vo;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class RespRailStationPrediction {
	
	@SerializedName("Trains")
	private List<AIMPredictionTrainInfo> aimPredictinoTrainInfo;

	public List<AIMPredictionTrainInfo> getAimPredictinoTrainInfo() {
		return aimPredictinoTrainInfo;
	}

	public void setAimPredictinoTrainInfo(
			List<AIMPredictionTrainInfo> aimPredictinoTrainInfo) {
		this.aimPredictinoTrainInfo = aimPredictinoTrainInfo;
	}

}
