package com.metrotransit.us.dc.vo;

import com.google.gson.annotations.SerializedName;

public class BusInfo {
	
	@SerializedName("DirectionNum")
	String directionNum;
	
	@SerializedName("DirectionText")
	String directionText;
	
	@SerializedName("Minutes")
	String mins;
	
	@SerializedName("RouteID")
	String routeId;
	
	@SerializedName("VehicleID")
	String vehicleId;

	public String getDirectionNum() {
		return directionNum;
	}

	public void setDirectionNum(String directionNum) {
		this.directionNum = directionNum;
	}

	public String getDirectionText() {
		return directionText;
	}

	public void setDirectionText(String directionText) {
		this.directionText = directionText;
	}

	public String getMins() {
		return mins;
	}

	public void setMins(String mins) {
		this.mins = mins;
	}

	public String getRouteId() {
		return routeId;
	}

	public void setRouteId(String routeId) {
		this.routeId = routeId;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}
	
}
