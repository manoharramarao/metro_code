package com.metrotransit.us.dc.common;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;

public class ServiceBootComplete extends IntentService {

	private static final String TAG = "ServiceBootComplete";

	/**
	 * 
	 */
	public ServiceBootComplete() {
		super("ServiceBootComplete");
		Log.d(TAG, "ServiceBootComplete");
	}

	/**
	 * 
	 * @param _name
	 */
	public ServiceBootComplete(String _name) {
		super(_name);
		Log.d(TAG, "ServiceBootComplete(_name)");
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate();
	}

	@Override
	protected void onHandleIntent(Intent _intent) {
		Log.d(TAG, "onHandleIntent");
		
		// making this false, so that when next time user open app, it should succeessfuly register ReceiverDeviceScreenState
		ApplicationMetro appMetro = (ApplicationMetro) getApplication();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences.Editor prefsEditor = prefs.edit();
		prefsEditor.putBoolean(getResources().getString(R.string.pref_are_receivers_regisered),
				false);
		prefsEditor.apply();
		
		// TODO figure out how to register receiver for device screen state on off when phone reboots.
		/*appMetro.registerReceivers();*/

		appMetro.startMetroAlarms();
		
		// refresh widgets
		appMetro.refreshWidgets();

		// start alarms
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
}
