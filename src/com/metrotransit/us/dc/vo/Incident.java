package com.metrotransit.us.dc.vo;

import com.google.gson.annotations.SerializedName;

/**
 * Represents incident of Rail.
 * 
 * @author manohar
 *
 */
public class Incident {
	
	/**
	 * ID of the incident.
	 */
	@SerializedName("IncidentID")
	private String incidentId;
	
	/**
	 * ID of the incident.
	 */
	@SerializedName("IncidentType")
	private String incidentType;
	
	/**
	 * Date and time where information was updated.
	 */
	@SerializedName("DateUpdated")
	private String dateUpdated;
	
	/**
	 * Severity of delay (if any). Can be "Minor", "Major", "Medium".
	 */
	@SerializedName("DelaySeverity")
	private String delaySeverity;
	
	/**
	 * Description what happened.
	 */
	@SerializedName("Description")
	private String description;
	
	/**
	 * Some text for emergency (if any).
	 */
	@SerializedName("EmergencyText")
	private String emergencyText;
	
	/**
	 * Station where delay starts.
	 */
	@SerializedName("StartLocationFullName")
	private String startLocationFullName;
	
	/**
	 * Station where delay ends. If null, then incident belongs only to StartLocationFullName station.
	 */
	@SerializedName("EndLocationFullName")
	private String endLocationFullName;
	
	/**
	 * List of lines affected by the incident. Separated by ";" Example: "RD; YL; BL;"
	 */
	@SerializedName("LinesAffected")
	private String linesAffected;
	
	/**
	 * Delay in minutes.
	 */
	@SerializedName("PassengerDelay")
	private String passengerDelay;

	/**
	 * @return {@link Incident#incidentId}
	 */
	public String getIncidentId() {
		return incidentId;
	}

	/**
	 * Sets {@link Incident#incidentId}
	 * @param incidentId -> incident ID
	 */
	public void setIncidentId(String incidentId) {
		this.incidentId = incidentId;
	}

	/**
	 * 
	 * @return {@link Incident#incidentType}
	 */
	public String getIncidentType() {
		return incidentType;
	}

	/**
	 * sets {@link Incident#incidentType}
	 * @param incidentType
	 */
	public void setIncidentType(String incidentType) {
		this.incidentType = incidentType;
	}

	/**
	 * 
	 * @return {@link Incident#dateUpdated} 
	 */
	public String getDateUpdated() {
		return dateUpdated;
	}

	/**
	 * sets {@link Incident#dateUpdated}
	 * @param dateUpdated
	 */
	public void setDateUpdated(String dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	/**
	 * 
	 * @return {@link Incident#delaySeverity}
	 */
	public String getDelaySeverity() {
		return delaySeverity;
	}

	/**
	 * sets {@link Incident#delaySeverity}
	 * @param delaySeverity
	 */
	public void setDelaySeverity(String delaySeverity) {
		this.delaySeverity = delaySeverity;
	}

	/**
	 *  
	 * @return {@link Incident#description}
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets {@link Incident#description}
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 
	 * @return {@link Incident#emergencyText}
	 */
	public String getEmergencyText() {
		return emergencyText;
	}

	/**
	 * sets {@link Incident#emergencyText}
	 * @param emergencyText
	 */
	public void setEmergencyText(String emergencyText) {
		this.emergencyText = emergencyText;
	}

	/**
	 * 
	 * @return {@link Incident#startLocationFullName}
	 */
	public String getStartLocationFullName() {
		return startLocationFullName;
	}

	/**
	 * sets {@link Incident#startLocationFullName}
	 * @param startLocationFullName
	 */
	public void setStartLocationFullName(String startLocationFullName) {
		this.startLocationFullName = startLocationFullName;
	}

	/**
	 * 
	 * @return {@link Incident#endLocationFullName}
	 */
	public String getEndLocationFullName() {
		return endLocationFullName;
	}

	/**
	 * sets {@link Incident#endLocationFullName}
	 * @param endLocationFullName
	 */
	public void setEndLocationFullName(String endLocationFullName) {
		this.endLocationFullName = endLocationFullName;
	}

	/**
	 * 
	 * @return {@link Incident#linesAffected}
	 */
	public String getLinesAffected() {
		return linesAffected;
	}

	/**
	 * sets {@link Incident#linesAffected}
	 * @param linesAffected
	 */
	public void setLinesAffected(String linesAffected) {
		this.linesAffected = linesAffected;
	}

	/**
	 * 
	 * @return {@link Incident#passengerDelay}
	 */
	public String getPassengerDelay() {
		return passengerDelay;
	}

	/**
	 * sets {@link Incident#passengerDelay}
	 * @param passengerDelay
	 */
	public void setPassengerDelay(String passengerDelay) {
		this.passengerDelay = passengerDelay;
	}
	
}
