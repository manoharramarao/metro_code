package com.metrotransit.us.dc.common;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.appwidget.PredictTrainsListWidget;
import com.metrotransit.us.dc.providers.ProviderWidgetDetails;
import com.metrotransit.us.dc.tables.WidgetDetails;

public class ServiceRefreshWidgets extends IntentService {

	private static final String TAG = "ServiceRefreshWidgets";

	/**
	 * 
	 */
	public ServiceRefreshWidgets() {
		super("ServiceRefreshWidgets");
		Log.d(TAG, "ServiceRefreshWidgets");
	}

	/**
	 * 
	 * @param _name
	 */
	public ServiceRefreshWidgets(String _name) {
		super(_name);
		Log.d(TAG, "ServiceRefreshWidgets(_name)");
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate();
	}

	@Override
	protected void onHandleIntent(Intent _intent) {
		Log.d(TAG, "onHandleIntent");
		AppWidgetManager appWidgetManager = AppWidgetManager
				.getInstance(getApplicationContext());
		// get all the rows from widget_details table
		ContentResolver cr = this.getApplicationContext().getContentResolver();
		Cursor cursor = cr.query(ProviderWidgetDetails.CONTENT_URI, null, null, null, null);
		Log.d(TAG, "Total no.of rows in widget_details table = " + cursor.getCount());

		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			do {
				if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(WidgetDetails.COLUMN_AUTO_REFRESH))) == 1) {
					int appWidgetId = Integer.parseInt(cursor.getString(cursor
							.getColumnIndex(WidgetDetails.COLUMN_WIDGET_ID)));
					/* Log.d(TAG, "widgetId = " + appWidgetId + "with freq = " + updateFreq); */
					PredictTrainsListWidget.updateAppWidget(getApplicationContext(), appWidgetManager, appWidgetId);
				}
			} while (cursor.moveToNext());
		}
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
}
