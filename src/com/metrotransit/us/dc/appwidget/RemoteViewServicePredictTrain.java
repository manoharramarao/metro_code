package com.metrotransit.us.dc.appwidget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.core.DCMetroConstants;
import com.metrotransit.us.dc.providers.ProviderPredictedTrains;
import com.metrotransit.us.dc.tables.USDCTrain;

/**
 * 
 * @author manohar
 *
 */
public class RemoteViewServicePredictTrain extends RemoteViewsService {

	private static final String TAG = "RemoteViewServicePredictTrain";
	
	@Override
	public RemoteViewsFactory onGetViewFactory(Intent intent) {
		Log.d(TAG, "onGetViewFactory");
		return new PredictTrainRemoteViewFactory(getApplicationContext(), intent);
	}

	class PredictTrainRemoteViewFactory implements RemoteViewsFactory{

		private static final String SUBTAG = "RemoteViewServicePredictTrain.PredictTrainRemoteViewFactory";
		private Context context;
		private Cursor c;
		int appWidgetId;
		public PredictTrainRemoteViewFactory(Context _context, Intent _intent){
			Log.d(SUBTAG, "PredictTrainRemoteViewFactory");
			this.context = _context;
			this.appWidgetId = _intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
			appWidgetId = Integer.valueOf(_intent.getData().getSchemeSpecificPart());
			Log.d(SUBTAG, "app widget id last but one= " + appWidgetId);
		}
		
		private Cursor executeQuery(){
			Log.d(SUBTAG, "executeQuery");
			String[] projection = new String[]{
					USDCTrain.COLUMN_ID,
					USDCTrain.COLUMN_APP_WIDGET_ID,
					USDCTrain.COLUMN_DESTINATION_STATION,
					USDCTrain.COLUMN_MINS,
					USDCTrain.COLUMN_CARS,
					USDCTrain.COLUMN_LINE
			};
			Log.d(SUBTAG, "app widget id last = " + appWidgetId);
			return getApplicationContext().getContentResolver().query(
					ProviderPredictedTrains.CONTENT_URI, projection, USDCTrain.COLUMN_APP_WIDGET_ID + "=" + appWidgetId,
					null, null);
		}
		
		
		public int getCount() {
			Log.d(SUBTAG, "getCount");
			return (c != null) ? c.getCount() : 0;
		}

		public long getItemId(int index) {
			Log.d(SUBTAG, "getItemId");
			return (c != null) ? c.getLong(c
					.getColumnIndex(USDCTrain.COLUMN_ID)) : index;
		}

		public RemoteViews getLoadingView() {
			Log.d(SUBTAG, "getLoadingView");
			// TODO Fix me - In nexus 4, it was showing progress bar even after fetching data. So commenting this out. This needs to be fixed
			/*RemoteViews views = new RemoteViews(getApplication()
					.getPackageName(), R.layout.app_widget_loading);
			return views;*/
			return null;
		}

		public RemoteViews getViewAt(int position) {
			Log.d(SUBTAG, "getViewAt");
			// move cursor to the required position
			c.moveToPosition(position);
			
			// get column indexes
			int idIdx = c.getColumnIndex(USDCTrain.COLUMN_ID);
			//int sourceStationIdx = c.getColumnIndex(ProviderPredictedTrains.KEY_SOURCE_STATION);
			int destStationIdx = c.getColumnIndex(USDCTrain.COLUMN_DESTINATION_STATION);
			int numOfMinsIdx = c.getColumnIndex(USDCTrain.COLUMN_MINS);
			//int numOfCarsIdx = c.getColumnIndex(ProviderPredictedTrains.KEY_NUM_OF_CARS);
			int lineIdx = c.getColumnIndex(USDCTrain.COLUMN_LINE);
			
			// get values
			String id = c.getString(idIdx);
			//String sourceStation = c.getString(sourceStationIdx);
			String destStation = c.getString(destStationIdx);
			String numOfMins = c.getString(numOfMinsIdx);
			//String numOfCars = c.getString(numOfCarsIdx);
			String line = c.getString(lineIdx);
			
			// create a new remote views object and use it to populate the
			// layout used to represent each train item info in the list.
			RemoteViews rv = new RemoteViews(context.getPackageName(),
					R.layout.appwidget_train_item);
			
			Log.d(SUBTAG, "line color is " + line);
			if(line.equalsIgnoreCase("RD")){
				rv.setInt(R.id.line, "setBackgroundColor", Color.RED);
			}else if(line.equalsIgnoreCase("or")){
				rv.setInt(R.id.line, "setBackgroundColor", getResources().getColor(R.color.orangeLine));
			}else if(line.equalsIgnoreCase("yl")){
				rv.setInt(R.id.line, "setBackgroundColor", Color.YELLOW);
			}else if(line.equalsIgnoreCase("BL")){
				rv.setInt(R.id.line, "setBackgroundColor", Color.BLUE);
			}else if(line.equalsIgnoreCase("gr")){
				rv.setInt(R.id.line, "setBackgroundColor", Color.GREEN);
			}else{
				rv.setInt(R.id.line, "setBackgroundColor", Color.TRANSPARENT);
			}
			
			rv.setTextViewText(R.id.widget_destination, destStation);
			
			rv.setTextViewText(R.id.widget_mins,
					(numOfMins.equalsIgnoreCase(DCMetroConstants.BRD)
							|| numOfMins.equalsIgnoreCase(DCMetroConstants.ARR)
							|| TextUtils.isEmpty(numOfMins) ? numOfMins
							: numOfMins + " " + DCMetroConstants.MINS));
			
			
			Intent fillInIntent = new Intent();
			Uri uri = Uri.withAppendedPath(ProviderPredictedTrains.CONTENT_URI, id);
			fillInIntent.setData(uri);
			
			rv.setOnClickFillInIntent(R.id.widget_destination, fillInIntent);
			rv.setOnClickFillInIntent(R.id.widget_mins, fillInIntent);
			
			return rv;
		}

		/**
		 * You should return the number of different of types of views in that row. In our case, since we are using only
		 * textview, we are returning 1. If there was an image and text then it would have been 2 and so on...
		 */
		public int getViewTypeCount() {
			Log.d(SUBTAG, "getViewTypeCount");
			// This is something you should know. return zero here and run in 4.1 and below. App widget works fine.
			// But keep zero and run in 4.2 (API level 17), it just shows default loading view.
			return 1;
		}

		public boolean hasStableIds() {
			Log.d(SUBTAG, "hasStableIds");
			return true;
		}

		public void onCreate() {
			Log.d(SUBTAG, "onCreate");
			((ApplicationMetro)getApplication()).enableStrictMode();
			c = executeQuery();
		}

		public void onDataSetChanged() {
			Log.d(SUBTAG, "onDataSetChanged");
			c = executeQuery();
		}

		public void onDestroy() {
			Log.d(SUBTAG, "onDestroy");
			c.close();
		}
		
	}
}
