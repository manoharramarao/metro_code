package com.metrotransit.us.dc.fragments;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.google.gson.Gson;
import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.adapters.AdapterAIMPredictedTrainInfo;
import com.metrotransit.us.dc.core.DCMetroConstants;
import com.metrotransit.us.dc.core.USDCMetroClient;
import com.metrotransit.us.dc.core.USDCMetroClientImpl;
import com.metrotransit.us.dc.vo.AIMPredictionTrainInfo;
import com.metrotransit.us.dc.vo.RespRailStationPrediction;

public class FragmentTrainPrediction extends Fragment implements OnItemClickListener, OnEditorActionListener{
	
	public static final String TAG = "FragmentTrainPrediction";
	
	private AutoCompleteTextView autoCompleteTextViewStationName;
	private ProgressBar progressBar;
	private String stationName;
	private ListView listViewTrainInfo;
	private TextView emptyView;
	private Button bClear;
	// TODO move it to MetroApplication class which is available across
	// application
	private USDCMetroClient dcMetroClient;
	private TrainPrediction trainPrediction;
	
	// To store the result and error string of API call
	String result;

	List<AIMPredictionTrainInfo> trainInfoList;
	private AdapterAIMPredictedTrainInfo adapterAIMPredictedTrainInfo;

	@Override
	public void onAttach(Activity activity){
		Log.d(TAG, "onAttach");
		super.onAttach(activity);
		if(activity instanceof TrainPrediction){
			trainPrediction = (TrainPrediction)activity;
		}else{
			throw new ClassCastException(activity.toString() + " must implement TrainPrediction.onStationSelected");
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getActivity().getApplication()).enableStrictMode();
		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
		/*setRetainInstance(true);*/
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
		Log.d(TAG, "onCreateOptionsMenu");
		super.onCreateOptionsMenu(menu, inflater);
	    inflater.inflate(R.menu.refresh_menu, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		Log.d(TAG, "onOptionsItemSelected");
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			Log.d(TAG, "refresh menu button clicked");
			emptyView.setVisibility(View.GONE);
			progressBar.setVisibility(View.VISIBLE);
			listViewTrainInfo.setEmptyView(progressBar);
			stationName = autoCompleteTextViewStationName.getText().toString().trim();
			trainPrediction.onStationSelected(stationName);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.d(TAG, "onCreateView");
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_train_prediction, container, false);
		view.requestFocus();
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		Log.d(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
		getActivity().getActionBar().setTitle(getResources().getString(R.string.next_train));
		
		// register views
		progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBar1);

		autoCompleteTextViewStationName = (AutoCompleteTextView) getActivity().findViewById(
				R.id.autoCompleteTextViewStationName);
		String[] stations = getResources().getStringArray(R.array.us_dc_stations);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.list_stations, stations);
		autoCompleteTextViewStationName.setAdapter(adapter);
		autoCompleteTextViewStationName.setOnItemClickListener(this);
		autoCompleteTextViewStationName.setOnEditorActionListener(this); 

		listViewTrainInfo = (ListView) getActivity().findViewById(R.id.listViewTrainInfo);
		emptyView = (TextView) getActivity().findViewById(R.id.next_train_empty_view);

		bClear = (Button) getActivity().findViewById(R.id.b_clear);
		bClear.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.d(TAG, "asking to clear text");
				autoCompleteTextViewStationName.setText("");
				((ApplicationMetro) (getActivity().getApplication())).showKeyboard();
			}
		});
		
		if(null != trainInfoList){
			paintDetails();
		}else{
			((ApplicationMetro)(getActivity().getApplication())).showKeyboard();
		}
	}
	
	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroyView(){
		Log.d(TAG, "onDestroyView");
		super.onDestroyView();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
	
	@Override
	public void onDetach(){
		Log.d(TAG, "onDetach");
		super.onDetach();
	}
	
	public interface TrainPrediction{
		public void onStationSelected(String stationName);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.d(TAG, "onItemClick");
		((ApplicationMetro) this.getActivity().getApplication()).hideKeyboard();
		emptyView.setVisibility(View.GONE);
		progressBar.setVisibility(View.VISIBLE);
		listViewTrainInfo.setEmptyView(progressBar);
		stationName = autoCompleteTextViewStationName.getText().toString().trim();
		trainPrediction.onStationSelected(stationName);
	}
	
	@Override
	public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
		Log.d(TAG, "onEditorAction");
		((ApplicationMetro) this.getActivity().getApplication()).hideKeyboard();
		emptyView.setVisibility(View.GONE);
		progressBar.setVisibility(View.VISIBLE);
		listViewTrainInfo.setEmptyView(progressBar);
		stationName = autoCompleteTextViewStationName.getText().toString().trim();
		trainPrediction.onStationSelected(stationName);
		return true;
	}
	
	class Train extends AsyncTask<String, Integer, String> {
		private final String SUBTAG = Train.class.getSimpleName();

		// I do the actvity of fetching train info from core components DCmetro
		// in background
		@Override
		protected String doInBackground(String... params) {
			Log.d(SUBTAG, "doInBackground");
			dcMetroClient = new USDCMetroClientImpl();
			result = getResources().getString(R.string.success);
			String stationName = params[0];
			boolean isPrecheckPass = doPreChecks(stationName);
			
			if(isPrecheckPass){
				String[] locationCodes = DCMetroConstants.locationNameCode.get(stationName);
				try {
					Gson gson = new Gson();
					RespRailStationPrediction respRailStationsPrediction = new RespRailStationPrediction();
					respRailStationsPrediction = gson.fromJson(dcMetroClient.predictTrain(getResources().getString(R.string.api_key), locationCodes), RespRailStationPrediction.class);
					trainInfoList = new ArrayList<AIMPredictionTrainInfo>();
					if (null != respRailStationsPrediction.getAimPredictinoTrainInfo() && !respRailStationsPrediction.getAimPredictinoTrainInfo().isEmpty()) {
						for (AIMPredictionTrainInfo trainInfo : respRailStationsPrediction.getAimPredictinoTrainInfo()) {
							if (null != trainInfo.getDestinationName() && !trainInfo.getDestinationName().isEmpty()) {
								trainInfoList.add(trainInfo);
								Log.d(SUBTAG, trainInfo.getDestinationName());
							}
						}
					} else if(trainInfoList == null || trainInfoList.isEmpty() ){
						result = getResources().getString(R.string.empty_view_text);
					}
					respRailStationsPrediction = null;
				} catch (MalformedURLException e) {
					e.printStackTrace();
					result = getResources().getString(R.string.int_conn_fail);
				} catch (IOException e) {
					e.printStackTrace();
					result = getResources().getString(R.string.int_conn_fail);
				}
			}
			return result;
		}
		
		boolean doPreChecks(String stationName){
			Log.d(SUBTAG, "doPreChecks");
			boolean isPreCheckPass = false;
			String[] locationCodes = DCMetroConstants.locationNameCode.get(stationName);
			if (stationName == null || stationName.isEmpty()) {
				result = getResources().getString(R.string.enter_station_name);
			}else if (locationCodes == null || locationCodes.length < 0) {
				result = getResources().getString(R.string.wrong_station_name);
			}
			else if (!((ApplicationMetro)getActivity().getApplication()).isConnectedToNetwork()) {
				result = getResources().getString(R.string.int_conn_fail);
			}else{
				isPreCheckPass = true;
			}
			return isPreCheckPass;
		}
		
		// I will be called when there is some status to update
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			/* progressBar.setVisibility(View.VISIBLE); */
			/* listViewTrainInfo.setVisibility(View.GONE); */
		}

		// I will be called once the background activity is over
		@Override
		protected void onPostExecute(String result) {
			Log.d(SUBTAG, "onPostExecute");
			paintDetails();
		}
	}
	
	public void paintDetails() {

		Log.d(TAG, "paintDetails");
		emptyView.setVisibility(View.GONE);
		progressBar.setVisibility(View.GONE);
		// paint the info
		if (null != trainInfoList && !trainInfoList.isEmpty()) {
			listViewTrainInfo.setVisibility(View.VISIBLE);
			adapterAIMPredictedTrainInfo = new AdapterAIMPredictedTrainInfo(getActivity(), trainInfoList);
			listViewTrainInfo.setAdapter(adapterAIMPredictedTrainInfo);
			// get list animation and start animating list
			Animation drawFromBottom = AnimationUtils.loadAnimation(getActivity(), R.anim.train_info_list);
			listViewTrainInfo.startAnimation(drawFromBottom);
		} else {
			emptyView.setText(result);
			emptyView.setVisibility(View.VISIBLE);
			listViewTrainInfo.setEmptyView(emptyView);
		}

	}
	
	public void updateStationInfo(String stationName){
		Log.d(TAG, "updateStationInfo");
		// clear off old values if set
		trainInfoList = null;
		listViewTrainInfo.setAdapter(new AdapterAIMPredictedTrainInfo(getActivity(), new ArrayList()));
		new Train().execute(stationName);
	}
}
