package com.metrotransit.us.dc.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Tell my geek {@link #onReceive(Context, Intent)} what needs to be done when phones configuration is changed. Like when orientation changes
 * 
 * @author manohar
 * 
 */
public class ReceiverConfigChanged extends BroadcastReceiver{

	private static final String TAG = "ReceiverConfigChanged";
	
	/**
	 * Tell me what needs to be done when device's configuration change, like orientation changes. As of now I call
	 * {@link ServiceConfigChanged}
	 * 
	 * @param _context
	 * @param _intent
	 */
	@Override
	public void onReceive(Context _context, Intent _intent) {
		
		Log.d(TAG, "onReceive");
		/*Toast.makeText(_context, "metro ReceiverConfigChanged received", Toast.LENGTH_LONG).show();*/
		Intent intent = new Intent(_context.getApplicationContext(), ServiceConfigChanged.class);
		_context.getApplicationContext().startService(intent);
		/*ApplicationMetro appMetro = (ApplicationMetro) getApplication();
		appMetro.refreshWidgets();*/
		/*Intent intent = new Intent(_context.getApplicationContext(), ServiceBootComplete.class);
		_context.getApplicationContext().startService(intent);*/
	}
}
