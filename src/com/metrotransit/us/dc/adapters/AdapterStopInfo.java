package com.metrotransit.us.dc.adapters;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.metrotransit.R;
import com.metrotransit.us.dc.vo.BusInfo;
import com.metrotransit.us.dc.vo.RespBusPrediction;

/**
 * Bridget between fragment_bus_prediction.lv_stop_info and {@link RespBusPrediction}
 * @author manohar
 *
 */
public class AdapterStopInfo extends ArrayAdapter{
	
	private final Activity activity;
	private final List stopInfo;
	
	@SuppressWarnings("unchecked")
	public AdapterStopInfo(Activity activity, List objects){
		super(activity, R.layout.stop_info_row, objects);
		this.activity = activity;
		this.stopInfo = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View rowView = convertView;
		StopInfoRow stopInfoRow = null;
		
		if(null == rowView){
			// Get a new instance of the row layout view
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.stop_info_row, parent, false);
			
			// Hold the view objects in an object, so they don't need to be
			// re-fetched
			stopInfoRow = new StopInfoRow();
			stopInfoRow.directionText = (TextView) rowView.findViewById(R.id.tv_direction_text);
			stopInfoRow.mins = (TextView) rowView.findViewById(R.id.tv_mins);
			stopInfoRow.routeId = (TextView) rowView.findViewById(R.id.tv_route_id);
			
			// Cache the view objects in the tag, so they can be re-accessed
			// later
			rowView.setTag(stopInfoRow);
		}else{
			stopInfoRow = (StopInfoRow) rowView.getTag();
		}
		
		BusInfo predictedBusInfo = (BusInfo) stopInfo.get(position);
		
		if(position != 0){
			BusInfo previousRow = (BusInfo) stopInfo.get(position-1);
			if(predictedBusInfo.getRouteId().equalsIgnoreCase(previousRow.getRouteId())){
				stopInfoRow.routeId.setVisibility(View.GONE);
			}else{
				stopInfoRow.routeId.setVisibility(View.VISIBLE);
			}
		}else{
			stopInfoRow.routeId.setVisibility(View.VISIBLE);
		}
		stopInfoRow.directionText.setText(predictedBusInfo.getDirectionText());
		stopInfoRow.mins.setText(predictedBusInfo.getMins() + " Mins");
		stopInfoRow.routeId.setText("Route " + predictedBusInfo.getRouteId());
		return rowView;
	}
	
	@Override
	public boolean isEnabled(int position){
		return false;
	}
	
	protected static class StopInfoRow {
		protected TextView routeId;
		protected TextView directionText;
		protected TextView mins;
	}
}

