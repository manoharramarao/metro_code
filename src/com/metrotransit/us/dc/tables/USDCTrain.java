package com.metrotransit.us.dc.tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class USDCTrain {
	
	private static final String TAG = "USDCTrain";
	
	public static final String TABLE_USDCTRAINS = "us_dc_train";
	
	// column names
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_APP_WIDGET_ID = "widget_id";
	/*public static final String COLUMN_SOURCE_STATION = "source_station";*/
	public static final String COLUMN_DESTINATION_STATION = "destination_station";
	public static final String COLUMN_CARS = "cars";
	public static final String COLUMN_LINE = "line";
	public static final String COLUMN_MINS = "mins";
	
	private static final String DATABASE_CREATE = "create table " + TABLE_USDCTRAINS + " ("
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_APP_WIDGET_ID + " integer, "
			+ COLUMN_DESTINATION_STATION + " text, "
			+ COLUMN_MINS + " text, "
			+ COLUMN_CARS + " text, "
			+ COLUMN_LINE + " text);";
	
	/**
	 * 
	 * @param db
	 */
	public static void onCreate(SQLiteDatabase db){
		Log.d(TAG, "onCreate");
		db.execSQL(DATABASE_CREATE);
	}
	
	/**
	 * 
	 * @param db
	 * @param oldVersion
	 * @param newVersion
	 */
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		Log.d(TAG, "onUpgrade");
		Log.w(TAG, "upgrading from version " + oldVersion + " to " + newVersion);
		db.execSQL("drop table if exists " + TABLE_USDCTRAINS);
		onCreate(db);
	}
}
