package com.metrotransit.us.dc.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import android.app.IntentService;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.gson.Gson;
import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.appwidget.PredictTrainsListWidget;
import com.metrotransit.us.dc.common.MetroDBHelper;
import com.metrotransit.us.dc.core.DCMetroConstants;
import com.metrotransit.us.dc.core.USDCMetroClientImpl;
import com.metrotransit.us.dc.providers.ProviderPredictedTrains;
import com.metrotransit.us.dc.providers.ProviderWidgetDetails;
import com.metrotransit.us.dc.tables.USDCTrain;
import com.metrotransit.us.dc.tables.WidgetDetails;
import com.metrotransit.us.dc.vo.AIMPredictionTrainInfo;
import com.metrotransit.us.dc.vo.RespRailStationPrediction;

public class ServiceTrainPrediction extends IntentService{

	static final String TAG = ServiceTrainPrediction.class.getName();
	public static final String PREDICTED_TRAINS_REFRESHED = "com.metrotransit.us.dc.PREDICTED_TRAINS_REFRESHED";
	int[] appWidgetIds;
	String locationName;
	List<AIMPredictionTrainInfo> trainInfoList;
	
	private ContentResolver cr;
	
	public ServiceTrainPrediction(){
		super("ServiceTrainPrediction");
		Log.d(TAG, "ServiceTrainPrediction");
	}
	
	public ServiceTrainPrediction(String _name){
		super(_name);
		Log.d(TAG, "ServiceTrainPrediction(name)");
	}
	
	public void onCreate() {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate();
	}

	/**
	 * @Description
	 * <li>Get preferences</li>
	 * <li>call wmata</li>
	 * <li>update heading of the widget</li>
	 * <li>notify app widget to update list view</li>
	 */
	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "onHandleIntent");
		
		Bundle extras = intent.getExtras();
		appWidgetIds = extras.getIntArray(AppWidgetManager.EXTRA_APPWIDGET_IDS);
		
		Context context = getApplicationContext();
		for(int appWidgetId : appWidgetIds){
			Log.d(TAG, "app widget id " + appWidgetId);
			// get the station name for above widget
			Cursor cursor = context.getContentResolver().query(ProviderWidgetDetails.CONTENT_URI, null,
					WidgetDetails.COLUMN_WIDGET_ID + "=" + appWidgetId, null, null);
			if(cursor != null && cursor.getCount() > 0){
				cursor.moveToFirst();
				locationName = cursor.getString(cursor.getColumnIndex(WidgetDetails.COLUMN_STATION));
				cursor.close();
			}
			
			// call wmata
			// TODO check if you have to make this call in a seperate thread
			predictTrain(appWidgetId);

			// As of now nobody is listening on my message. This is for future.
			sendBroadcast(new Intent(PREDICTED_TRAINS_REFRESHED));
			
			// Notify remote view to update list of trains
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
			appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,R.id.list_view_trains);
		}
	}
	
	public RemoteViews buildUpdatedHeading(Context _context){
		Log.d(TAG, "buildUpdatedHeading");
		RemoteViews views = new RemoteViews(_context.getPackageName(), R.layout.appwidget_layout_train);
		views.setEmptyView(R.id.list_view_trains, R.id.text_view_empty_view);
		views.setTextViewText(R.id.text_view_heading, locationName);
		return views;
	}
	
	private void predictTrain(int appWidgetId){
		// call wmata and get the list of trains for the given station
		Log.d(TAG, "predictTrain");
		
		USDCMetroClientImpl dcMetroClient = new USDCMetroClientImpl();
		if(!TextUtils.isEmpty(locationName)){
			String[] locationCodes = DCMetroConstants.locationNameCode
					.get(locationName);
			try {
				Gson gson = new Gson();
				RespRailStationPrediction respRailStationsPrediction = new RespRailStationPrediction();
				if(((ApplicationMetro)getApplication()).isConnectedToNetwork()){
					respRailStationsPrediction = gson.fromJson(dcMetroClient
							.predictTrain(getString(R.string.api_key), locationCodes),
							RespRailStationPrediction.class);
				}else{
					// don't make http call. Just bail out
				}
				trainInfoList = new ArrayList<AIMPredictionTrainInfo>();
				if(null != respRailStationsPrediction.getAimPredictinoTrainInfo() && !respRailStationsPrediction
						.getAimPredictinoTrainInfo().isEmpty()){
					for (AIMPredictionTrainInfo trainInfo : respRailStationsPrediction
							.getAimPredictinoTrainInfo()) {
						if (null != trainInfo.getDestinationName()
								&& !trainInfo.getDestinationName().isEmpty()) {
							trainInfoList.add(trainInfo);
							Log.d(TAG, trainInfo.getDestinationName());
						}
					}
				}
				respRailStationsPrediction = null;
			} catch (MalformedURLException e) {
				Log.e(TAG, "Malformed Exception occurred");
			} catch (IOException e) {
				Log.e(TAG, "IOException occured");
			}finally{
				addNewlyPredictedTrains(appWidgetId);
			}
		}
	}
	
	private void addNewlyPredictedTrains(int appWidgetId) {
		
		Log.d(TAG, "addNewlyPredictedTrains");
		
		// populating DB if in case it is not done.
		MetroDBHelper dbHelper = new MetroDBHelper(getApplicationContext());
		dbHelper.createDB();
		
		cr = getContentResolver();
		ArrayList<ContentValues> trainsList = new ArrayList<ContentValues>();
		cr.delete(ProviderPredictedTrains.CONTENT_URI, USDCTrain.COLUMN_APP_WIDGET_ID + "=" + appWidgetId, null);
		if(trainInfoList!=null && !trainInfoList.isEmpty()){
			for (AIMPredictionTrainInfo trainInfo : trainInfoList) {
				ContentValues values = new ContentValues();
				values.put(USDCTrain.COLUMN_APP_WIDGET_ID, appWidgetId);
				values.put(USDCTrain.COLUMN_DESTINATION_STATION, trainInfo.getDestinationName());
				values.put(USDCTrain.COLUMN_LINE, trainInfo.getLine());
				values.put(USDCTrain.COLUMN_CARS, trainInfo.getNumOfCars());
				values.put(USDCTrain.COLUMN_MINS, trainInfo.getNumOfMins());
				trainsList.add(values);
			}
			cr.bulkInsert(ProviderPredictedTrains.CONTENT_URI,
					trainsList.toArray(new ContentValues[trainsList.size()]));
		}
	}
}
