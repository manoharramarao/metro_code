package com.metrotransit.us.dc.core;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import com.metrotransit.us.dc.vo.RespIncidents;

public interface USDCMetroClient {

	/**
	 * 
	 * @param apiKey
	 * @param locationCodes
	 * @return
	 * @throws IOException
	 */
	String predictTrain(String apiKey, String[] locationCodes)
			throws IOException;

	/**
	 * I connect to wmata and get the rail incidents.
	 * 
	 * @param apiKey
	 *            -> apiKey which allows me to talk to wmata services
	 * @return {@link RespIncidents}
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	RespIncidents getRailIncidents(String apiKey) throws MalformedURLException, IOException;
	
	/**
	 * 
	 * @param apiKey
	 * @param stopIds
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	String predictBus(String apiKey, List<String> stopIds) throws MalformedURLException, IOException;
}
