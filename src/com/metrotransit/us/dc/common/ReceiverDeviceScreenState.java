package com.metrotransit.us.dc.common;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.SystemClock;
import android.util.Log;

import com.metrotransit.us.dc.providers.ProviderWidgetDetails;
import com.metrotransit.us.dc.receivers.ReceiverPredictTrainAlarm;
import com.metrotransit.us.dc.services.ServiceTrainPrediction;
import com.metrotransit.us.dc.tables.WidgetDetails;

/**
 * 
 * @author manohar
 *
 */
public class ReceiverDeviceScreenState extends BroadcastReceiver{
	
	private AlarmManager alarmManager;
	private PendingIntent alarmIntent;

	private static final String TAG = "ReceiverDeviceScreenState";
	@Override
	public void onReceive(Context _context, Intent _intent) {
		
		Log.d(TAG, "onReceive");
		
		// get all the rows from widget_details table
		ContentResolver cr = _context.getContentResolver();
		Cursor cursor = cr.query(ProviderWidgetDetails.CONTENT_URI, null, null, null, null);
		Log.d(TAG, "Total no.of rows in widget_details table = " + cursor.getCount());
		
		alarmManager = (AlarmManager) _context.getSystemService(Context.ALARM_SERVICE);
		String ALARM_ACTION = ReceiverPredictTrainAlarm.ACTION_REFRESH_TRAIN_PREDICTION_ALARM;
		Intent intentToFire = new Intent(ALARM_ACTION);
		
		if(_intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
			Log.d(TAG, "Screen turned off");
			if(cursor != null && cursor.getCount() > 0){
				cursor.moveToFirst();
				do{
					int updateFreq = cursor.getInt(cursor.getColumnIndex(WidgetDetails.COLUMN_REFRESH_FREQ));
					alarmIntent = PendingIntent.getBroadcast(_context, updateFreq, intentToFire, 0);
					alarmManager.cancel(alarmIntent);
				}while(cursor.moveToNext());
			}
		}else if(_intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
			
			Log.d(TAG, "Screen turned on");
			
			// TODO move this entire implementation to a service. start the service here or where ever you want.
			int[] appWidgetIds = null;
			if(cursor != null && cursor.getCount() > 0){
				appWidgetIds = new int[cursor.getCount()];
				cursor.moveToFirst();
				int count = 0;
				do{
					appWidgetIds[count++] = Integer.parseInt(cursor.getString(cursor.getColumnIndex(WidgetDetails.COLUMN_WIDGET_ID)));
					int updateFreq = cursor.getInt(cursor.getColumnIndex(WidgetDetails.COLUMN_REFRESH_FREQ));
					long timeToRefresh = SystemClock.elapsedRealtime() + updateFreq	* 60 * 1000;
					PendingIntent alarmIntent = PendingIntent.getBroadcast(_context, updateFreq, intentToFire, 0);
					alarmManager.cancel(alarmIntent);
					alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME, timeToRefresh, updateFreq * 60 * 1000, alarmIntent);
				}while(cursor.moveToNext());
			}
			Log.d(TAG, "length of appWidgetIds = " + appWidgetIds.length);
			Intent serviceTrainPrediction = new Intent(_context, ServiceTrainPrediction.class);
			serviceTrainPrediction.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
			_context.startService(serviceTrainPrediction);
		}
	}
}
