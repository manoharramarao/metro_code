package com.metrotransit.us.dc.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.util.Log;

import com.metrotransit.R;

public class MetroEula {
	private String EULA_PREFIX = "eula_";
    private Activity activity;
    private Class<?> activityToStart;
    private static final String TAG = "MetroEula";
    
    public MetroEula(Activity _activity, Class<?> _activityToStart) {
        activity = _activity;
        activityToStart = _activityToStart;  
    }
 
    private PackageInfo getPackageInfo() {
        PackageInfo pi = null;
        try {
             pi = activity.getPackageManager().getPackageInfo(activity.getPackageName(), PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pi;
    }
 
     public void show() {
    	 Log.d(TAG, "show");
        PackageInfo versionInfo = getPackageInfo();
 
        // the eulaKey changes every time you increment the version number in the AndroidManifest.xml
        final String eulaKey = EULA_PREFIX + versionInfo.versionCode;
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);
        boolean hasBeenShown = prefs.getBoolean(eulaKey, false);
        if(hasBeenShown == false){
 
            // Show the Eula
            String title = activity.getString(R.string.app_name) + " v" + versionInfo.versionName + " " + activity.getString(R.string.disclaimer);
 
            //Includes the updates as well so users know what changed.
			String message = activity.getString(R.string.updates) + "\n\n" + activity.getString(R.string.eula);
 
            AlertDialog.Builder builder = new AlertDialog.Builder(activity)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(R.string.accept, new Dialog.OnClickListener() {
 
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Mark this version as read.
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putBoolean(eulaKey, true);
                            editor.commit();
                            dialogInterface.dismiss();
                            if(null != activityToStart){
                            	activity.startActivity(new Intent(activity.getApplicationContext(), activityToStart));
                            	activity.finish();
                            }
                        }
                    })
                    .setNegativeButton(R.string.reject, new Dialog.OnClickListener() {
 
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Close the activity as they have declined the Eula
                            activity.finish();
                        }
 
                    });
            AlertDialog eulaDialog = builder.create();
            eulaDialog.setCanceledOnTouchOutside(false);
            eulaDialog.setCancelable(false);
            eulaDialog.show();
        }else{
        	if(null != activityToStart){
            	activity.startActivity(new Intent(activity.getApplicationContext(), activityToStart));
            	activity.finish();
            }
        }
    }
}
