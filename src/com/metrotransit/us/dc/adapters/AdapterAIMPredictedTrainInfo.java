package com.metrotransit.us.dc.adapters;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.metrotransit.R;
import com.metrotransit.us.dc.vo.AIMPredictionTrainInfo;

/**
 * Bridge between fragment_train_prediction.listViewTrainInfo and {@link AIMPredictionTrainInfo}
 * @author manohar
 *
 */
public class AdapterAIMPredictedTrainInfo extends ArrayAdapter{
	
	private final Activity activity;
	private final List trains;
	
	@SuppressWarnings("unchecked")
	public AdapterAIMPredictedTrainInfo(Activity activity, List objects){
		super(activity, R.layout.train_info_row, objects);
		this.activity = activity;
		this.trains = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View rowView = convertView;
		TrainInfoRowView trainInfoRowView = null;
		
		if(null == rowView){
			// Get a new instance of the row layout view
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.train_info_row, parent, false);
			
			// Hold the view objects in an object, so they don't need to be
			// re-fetched
			trainInfoRowView = new TrainInfoRowView();
			trainInfoRowView.destination = (TextView) rowView.findViewById(R.id.textViewDestination);
			trainInfoRowView.cars = (TextView) rowView.findViewById(R.id.textViewCars);
			trainInfoRowView.mins = (TextView) rowView.findViewById(R.id.textViewMins);
			trainInfoRowView.rlColor = (FrameLayout) rowView.findViewById(R.id.frameLayoutColor);
			
			// Cache the view objects in the tag, so they can be re-accessed later
			rowView.setTag(trainInfoRowView);
		}else{
			trainInfoRowView = (TrainInfoRowView) rowView.getTag();
		}
		
		// Transfer the Train data from the data object to the view objects
		AIMPredictionTrainInfo trainInfo = (AIMPredictionTrainInfo) trains.get(position);
		trainInfoRowView.destination.setText(trainInfo.getDestinationName());
		if(null != trainInfo.getNumOfCars() && !trainInfo.getNumOfCars().equalsIgnoreCase("null")){
			trainInfoRowView.cars.setText(trainInfo.getNumOfCars() + " ");
		}else{
			// ignore
			trainInfoRowView.cars.setText("");
		}
		
		if(trainInfo.getNumOfMins().equalsIgnoreCase("brd")){
			trainInfoRowView.mins.setText("BRD");
		}else if(trainInfo.getNumOfMins().equalsIgnoreCase("arr")){
			trainInfoRowView.mins.setText("ARR");
		} else { 
			trainInfoRowView.mins.setText(trainInfo.getNumOfMins() + " Mins");
		}
		
		// TODO make it configurable. Tomorrow if new color is added, code
		// should not be changed and just the properties files should be
		trainInfoRowView.mins.setTextColor(Color.WHITE);
		if(trainInfo.getLine().equalsIgnoreCase("RD")){
			trainInfoRowView.rlColor.setBackgroundColor(activity.getResources().getColor(R.color.redLine));
		}else if(trainInfo.getLine().equalsIgnoreCase("or")){
			trainInfoRowView.rlColor.setBackgroundColor(activity
					.getResources().getColor(R.color.orangeLine));
		}else if(trainInfo.getLine().equalsIgnoreCase("yl")){
			trainInfoRowView.rlColor.setBackgroundColor(activity.getResources().getColor(R.color.yellowLine));
		}else if(trainInfo.getLine().equalsIgnoreCase("BL")){
			trainInfoRowView.rlColor.setBackgroundColor(activity.getResources().getColor(R.color.blueLine));
		}else if(trainInfo.getLine().equalsIgnoreCase("gr")){
			trainInfoRowView.rlColor.setBackgroundColor(activity.getResources().getColor(R.color.greenLine));
		}else{ // Don't use extra Dimag. Have this else block explicitly
			trainInfoRowView.mins.setTextColor(Color.BLACK);
		}
		return rowView;
	}
	
	// This is to make listview non-clickable
	@Override
	public boolean isEnabled(int position){
		return false;
	}
	
	protected static class TrainInfoRowView {
		protected TextView destination;
		protected TextView cars;
		protected TextView mins;
		protected ImageView line;
		protected FrameLayout rlColor;
	}
}

