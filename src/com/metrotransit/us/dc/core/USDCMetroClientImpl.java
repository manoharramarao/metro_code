package com.metrotransit.us.dc.core;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.metrotransit.us.dc.vo.AIMPredictionTrainInfo;
import com.metrotransit.us.dc.vo.Incident;
import com.metrotransit.us.dc.vo.RespIncidents;

public class USDCMetroClientImpl implements USDCMetroClient{
	private static final String TAG = USDCMetroClientImpl.class.getName();
	
	// TODO make this to return RespRailStationPrediction
	public String predictTrain(String apiKey, String[] locationCodes) throws IOException {
		Log.d(TAG, "getRailStationPrediction");
		String apiUrl = "http://api.wmata.com/StationPrediction.svc/json/GetPrediction/";
		StringBuffer allLocationCodes = new StringBuffer(4);
		if(null != locationCodes && locationCodes.length > 0){
			allLocationCodes.append(locationCodes[0]);
			int i = 1;
			while(i < locationCodes.length){
				allLocationCodes.append(",").append(locationCodes[i++]);
			}
		}
		String url = apiUrl + allLocationCodes.toString() + "?api_key=" + apiKey;
		String response = getHTTPResponse(url);
		return response;
	}
	
	/**
	 * Though it is taking list of stopIds as parameter, it will return predictions for only the first stopID
	 */
	public String predictBus(String apiKey, List<String> stopIds) throws MalformedURLException, IOException {
		Log.d(TAG, "predictBus");
		String url = "http://api.wmata.com/NextBusService.svc/json/JPredictions?StopID=" + stopIds.get(0) + "&api_key="
				+ apiKey;
		String response = getHTTPResponse(url);
		return response;
	}
	
	
	private String getHTTPResponse(String uri) throws MalformedURLException, IOException{
		Log.d(TAG, "getHTTPResponse");
		HttpURLConnection connection = null;
		Log.d(TAG, "url is " + uri);
		connection = (HttpURLConnection) new URL(uri).openConnection();
		connection.setDoInput(true);
		connection.setReadTimeout(5000);
		InputStream inStream = null;
		inStream = connection.getInputStream();
		String response = toString(inStream);
        return response;
	}
	
	private static String toString(InputStream inputStream) {
        InputStreamReader reader;
        try {
                reader = new InputStreamReader(inputStream, "UTF-8");
        } catch (UnsupportedEncodingException e) {
                reader = new InputStreamReader(inputStream);
        }
        return toString(reader);
	}
	
	/**
     * Use a buffered reader to extract the contents of the given reader.
     * 
     * @param reader
     * @return The contents of this reader.
     */
    private static String toString(Reader reader) throws RuntimeException {
            try {
                    // Buffer if not already buffered
                    reader = reader instanceof BufferedReader ? (BufferedReader) reader : new BufferedReader(reader);
                    StringBuilder output = new StringBuilder();
                    while (true) {
                            int c = reader.read();
                            if (c == -1)
                                    break;
                            output.append((char) c);
                    }
                    return output.toString();
            } catch (IOException ex) {
                    throw new RuntimeException(ex);
            } finally {
                    close(reader);
            }
    }
    
    /**
     * Close a reader/writer/stream, ignoring any exceptions that result. Also
     * flushes if there is a flush() method.
     */
    private static void close(Closeable input) {
            if (input == null)
                    return;
            // Flush (annoying that this is not part of Closeable)
            try {
                    Method m = input.getClass().getMethod("flush");
                    m.invoke(input);
            } catch (Exception e) {
                    // Ignore
            }
            // Close
            try {
                    input.close();
            } catch (IOException e) {
                    // Ignore
            }
    }

    /**
     * 
     * @throws IOException 
     * @throws MalformedURLException 
     * @see {@link USDCMetroClient#getRailIncidents(String)}
     */
	
	public RespIncidents getRailIncidents(String apiKey) throws MalformedURLException, IOException {
		
		// make web service call and get the json
		String apiUrl = "http://api.wmata.com/Incidents.svc/json/Incidents";
		String url = apiUrl + "?api_key=" + apiKey;
		Log.d(USDCMetroClientImpl.class.getName(), "URL is " + url);
		
		String response = getHTTPResponse(url);
		// printing response to logs
		Log.d(USDCMetroClientImpl.class.getName(), "Response string is " + response );
		
		// parse json and prepare RespIncidents object
		Gson gson = new Gson();
		RespIncidents respIncidents = new RespIncidents();
		respIncidents = gson.fromJson(response, RespIncidents.class);
		
		return respIncidents;
	}

}
