package com.metrotransit.us.dc.providers;

import java.util.Arrays;
import java.util.HashSet;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.metrotransit.us.dc.common.MetroDBHelper;
import com.metrotransit.us.dc.tables.WidgetDetails;

public class ProviderWidgetDetails extends ContentProvider{

	private static final String TAG = "ProviderWidgetDetails";
	
	// db
	private MetroDBHelper db;
	
	// URI matcher
	private static final int WIDGET_DETAILS = 1;
	private static final int WIDGET_DETAILS_ID = 2;
	
	// Authority
	private static final String AUTHORITY = "com.metrotransit.us.dc.providers.widgetdetails";
	private static final String BASE_PATH = "ProviderWidgetDetails";
	
	// content://com.fundroid.myexperiments.database.contentproviders/USDCStop
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
	
	private static final UriMatcher URIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	static{
		URIMatcher.addURI(AUTHORITY, BASE_PATH, WIDGET_DETAILS);
		URIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", WIDGET_DETAILS_ID);
	}
	
	@Override
	public boolean onCreate() {
		Log.d(TAG, "onCreate");
		db = new MetroDBHelper(getContext());
		return false;
	}
	
	/**
	 * 1. check if requested columns are available 
	 * 2. set the table
	 * 3. match uri
	 * 4. build query accordingly
	 * 5. query database
	 * 6. set notification to notify the listeners
	 * 7. return cursor
	 * 
	 */
	@Override
	public Cursor query(Uri _uri, String[] _projection, String _selection, String[] _selectionArgs, String _sortOrder) {
	
		Log.d(TAG, "query");
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		
		checkColumns(_projection);
		queryBuilder.setTables(WidgetDetails.TABLE_WIDGET_DETAILS);
		int uriType = URIMatcher.match(_uri);
		switch(uriType){
		case WIDGET_DETAILS:
			// do nothing
			break;
		case WIDGET_DETAILS_ID:
			// add widgetID to the query
			queryBuilder.appendWhere(WidgetDetails.COLUMN_ID + "=" + _uri.getLastPathSegment());
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + _uri);
		}
		
		SQLiteDatabase database = db.getWritableDatabase();
		Cursor cursor = queryBuilder.query(database, _projection, _selection, _selectionArgs, null, null, _sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), _uri);
		
		return cursor;
	}
	
	@Override
	public String getType(Uri arg0) {
		Log.d(TAG, "getType");
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * 
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Log.d(TAG, "insert");
		int uriType = URIMatcher.match(uri);
		SQLiteDatabase database = db.getWritableDatabase();
		int rowsInserted = 0;
		long id = 0;
		switch(uriType){
		case WIDGET_DETAILS:
			id = database.insert(WidgetDetails.TABLE_WIDGET_DETAILS, null, values);
			break;
		default:
			throw new IllegalArgumentException("Unknown uri " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(BASE_PATH + "/" + id);
	}
	
	/**
	 * 
	 */
	@Override
	public int delete(Uri _uri, String selection, String[] selectionArgs) {
		Log.d(TAG, "delete");
		int uriType = URIMatcher.match(_uri);
		SQLiteDatabase database = db.getWritableDatabase();
		int rowsDeleted = 0;
		
		switch(uriType){
		case WIDGET_DETAILS:
			rowsDeleted = database.delete(WidgetDetails.TABLE_WIDGET_DETAILS, selection, selectionArgs);
			break;
		case WIDGET_DETAILS_ID:
			String id = _uri.getLastPathSegment();
			if(TextUtils.isEmpty(selection)){
				rowsDeleted = database.delete(WidgetDetails.TABLE_WIDGET_DETAILS, WidgetDetails.COLUMN_ID + "=" + id, null);
			} else {
				rowsDeleted = database.delete(WidgetDetails.TABLE_WIDGET_DETAILS, WidgetDetails.COLUMN_ID + "=" + id + "and" + selection, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + _uri);
		}
		getContext().getContentResolver().notifyChange(_uri, null);
		return rowsDeleted;
	}

	/**
	 * 
	 */
	@Override
	public int update(Uri _uri, ContentValues _values, String _selection, String[] _selectionArgs) {
		Log.d(TAG, "update");
		int uriType = URIMatcher.match(_uri);
		SQLiteDatabase database = db.getWritableDatabase();
		int rowsUpdated = 0;
		
		switch(uriType){
		case WIDGET_DETAILS:
			rowsUpdated = database.update(WidgetDetails.TABLE_WIDGET_DETAILS, _values, _selection, _selectionArgs);
			break;
		case WIDGET_DETAILS_ID:
			String id = _uri.getLastPathSegment();
			if(TextUtils.isEmpty(_selection)){
				rowsUpdated = database.update(WidgetDetails.TABLE_WIDGET_DETAILS, _values, WidgetDetails.COLUMN_ID + "=" + id, null);
			}else{
				rowsUpdated = database.update(WidgetDetails.TABLE_WIDGET_DETAILS, _values, WidgetDetails.COLUMN_ID + "=" + id
						+ " and " + _selection, _selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + _uri);
		}
		
		getContext().getContentResolver().notifyChange(_uri, null);
		return rowsUpdated;
	}
	
	/**
	 * 
	 * @param projection
	 */
	private void checkColumns(String[] projection){
		Log.d(TAG, "checkColumns");
		String[] columnsAvail = { WidgetDetails.COLUMN_ID, WidgetDetails.COLUMN_WIDGET_ID, WidgetDetails.COLUMN_STATION, WidgetDetails.COLUMN_REFRESH_FREQ, WidgetDetails.COLUMN_AUTO_REFRESH };
		
		if(projection != null){
			HashSet<String> columnsRequested = new HashSet<String>(Arrays.asList(projection));
			HashSet<String> columnsAvailable = new HashSet<String>(Arrays.asList(columnsAvail));
			
			if(!columnsAvailable.containsAll(columnsRequested)){
				throw new IllegalArgumentException("Columns requeseted not present");
			}
		}
	}
}
