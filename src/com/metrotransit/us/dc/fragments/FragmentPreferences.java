package com.metrotransit.us.dc.fragments;

import java.util.List;

import android.preference.PreferenceActivity;

import com.metrotransit.R;

public class FragmentPreferences extends PreferenceActivity{
	
	public static final String PREF_AUTO_UPDATE = "PREF_AUTO_UPDATE";
	public static final String PREF_UPDATE_FREQ = "PREF_UPDATE_FREQ";
	public static final String PREF_STATION_NAME = "PREF_STATION_NAME";
	
	@Override
	public void onBuildHeaders(List<Header> target){
		loadHeadersFromResource(R.xml.preference_headers, target);
	}
}
