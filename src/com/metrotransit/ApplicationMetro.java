package com.metrotransit;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.metrotransit.R;
import com.metrotransit.us.dc.common.ReceiverConfigChanged;
import com.metrotransit.us.dc.common.ReceiverDeviceScreenState;
import com.metrotransit.us.dc.common.ServiceRefreshWidgets;
import com.metrotransit.us.dc.providers.ProviderWidgetDetails;
import com.metrotransit.us.dc.receivers.ReceiverPredictTrainAlarm;
import com.metrotransit.us.dc.tables.WidgetDetails;

/**
 * Application class For Metro application. Application level operations are don here
 * <li>Check if device is connected to network</li>
 * <li>Register application level receivers. device screen state receiver, boot receiver</li>
 * <li>Refreshing all widgets</li>
 * @author manohar
 *
 */
public class ApplicationMetro extends Application{
	
	private static final String TAG = "ApplicationMetro";
	
	public void showKeyboard() {
		Log.d(TAG, "showKeyboard");
		// TODO Fix me - if keyboard is already shown, I am hiding it. I should not hide if it is already shown.
		InputMethodManager mgr = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
		/* mgr.hideSoftInputFromWindow(listViewTrainInfo.getWindowToken(), 0); */
		mgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	/**
	 * Will hide the keyboard
	 */
	public void hideKeyboard() {
		Log.d(TAG, "hideKeyboard");
		InputMethodManager mgr = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
		// TODO it is toggling and not actually hiding keyboard
		mgr.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
	}
	
	/**
	 * Tell you if device is connected to network
	 * @return
	 */
	public boolean isConnectedToNetwork(){
		final ConnectivityManager connectivityManager = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		// This if condition is required in the scenario where it was passing null value for either of one when as soon as device screen is on and device is not on network.
		// TODO fix me. in else you got to return false, but somehow it is not working. Analyze and fix
		if(null != wifi && null != mobile){
			return (wifi.isConnected() || mobile.isConnected())? true: false;
		}else{
			return true;
		}
	}

	/**
	 * I will register all receivers.
	 * <li>ReceiverDeviceScreenState - {@link ReceiverDeviceScreenState}</li>
	 * <li>ReceiverConfigChanged - {@link ReceiverConfigChanged}</li>
	 */
	public void registerReceivers(){
		Log.d(TAG, "registerReceivers");

		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		boolean areRecieversRegistered = prefs.getBoolean(
				getResources().getString(R.string.pref_are_receivers_regisered), false);
		Log.d(TAG, "value of areRecieversRegistered = " + areRecieversRegistered);
		// register device screen state receiver
		if(!areRecieversRegistered){
			/*IntentFilter filterScreen = new IntentFilter(Intent.ACTION_SCREEN_ON);
			filterScreen.addAction(Intent.ACTION_SCREEN_OFF);
			registerReceiver(new ReceiverDeviceScreenState(), filterScreen);*/
			
			IntentFilter filterConfigChange = new IntentFilter(Intent.ACTION_CONFIGURATION_CHANGED);
			registerReceiver(new ReceiverConfigChanged(), filterConfigChange);
			
			SharedPreferences.Editor prefsEditor = prefs.edit();
			prefsEditor.putBoolean(getResources().getString(R.string.pref_are_receivers_regisered),
					true);
			prefsEditor.apply();
		}
	}
	
	/**
	 * Refresh widgets with the data in DB. This will only update views and not the data. Refer
	 * {@link com.metrotransit.us.dc.common.ServiceRefreshWidgets}
	 */
	public void refreshWidgets() {
		Log.d(TAG, "refreshWidgets");
		startService(new Intent(getApplicationContext(), ServiceRefreshWidgets.class));
	}
	
	public void enableStrictMode(){
		PackageInfo packageInfo;
		try {
			packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			int flags = packageInfo.applicationInfo.flags;
			 if ((flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
		         StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
		                 .detectDiskReads()
		                 .detectDiskWrites()
		                 .detectNetwork()
		                 .penaltyLog()
		                 .build());
		     }
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void startMetroAlarms(){
		Cursor cursor = getContentResolver().query(ProviderWidgetDetails.CONTENT_URI, null, null, null, null);
		if(cursor != null && cursor.getCount()>0){
			do{
				int updateFreq = cursor.getInt(cursor.getColumnIndex(WidgetDetails.COLUMN_REFRESH_FREQ));
				
				AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
				String ALARM_ACTION = ReceiverPredictTrainAlarm.ACTION_REFRESH_TRAIN_PREDICTION_ALARM;
				int alarmType = AlarmManager.ELAPSED_REALTIME;
				long timeToRefresh = SystemClock.elapsedRealtime() + updateFreq * 60 * 1000;
				Intent intentToFire = new Intent(ALARM_ACTION);
				PendingIntent alarmIntent = PendingIntent.getBroadcast(getApplicationContext(), updateFreq, intentToFire, 0);
				alarmManager.setInexactRepeating(alarmType, timeToRefresh, updateFreq * 60 * 1000, alarmIntent);
			}while(cursor.moveToNext());
		}
	}
}
