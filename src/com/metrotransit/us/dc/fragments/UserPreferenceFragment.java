package com.metrotransit.us.dc.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.util.Log;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;

public class UserPreferenceFragment extends PreferenceFragment{
	public static final String TAG = "UserPreferenceFragment";
	@Override
	public void onCreate(Bundle _savedInstanceState){
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getActivity().getApplication()).enableStrictMode();
		super.onCreate(_savedInstanceState);
		addPreferencesFromResource(R.xml.user_preferences);
	}
}
