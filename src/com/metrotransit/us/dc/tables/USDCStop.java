package com.metrotransit.us.dc.tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class USDCStop {
	
	private static final String TAG = "USDCStop";
	
	public static final String TABLE_USDCSTOPS = "us_dc_stop";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_STOP_ID = "stop_id";
	
	
	/**
	 * 
	 */
	private static final String DATABASE_CREATE = "create table " + 
			TABLE_USDCSTOPS + 
			"(" + 
			COLUMN_ID + " integer primary key autoincrement, " +
			COLUMN_NAME + " text not null, " + 
			COLUMN_STOP_ID + " text not null, " + 
			");";
	
	/**
	 * 
	 * @param db
	 */
	public static void onCreate(SQLiteDatabase db){
		Log.d(TAG, "onCreate");
		db.execSQL(DATABASE_CREATE);
	}
	
	/**
	 * 
	 * @param db
	 * @param oldVersion
	 * @param newVersion
	 */
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		Log.d(TAG, "onUpgrade");
		Log.w(TAG, "upgrading from version " + oldVersion + " to " + newVersion);
		db.execSQL("drop table if exists " + TABLE_USDCSTOPS);
		onCreate(db);
	}
}
