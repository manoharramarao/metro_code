package com.metrotransit.us.dc.vo;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * I represent the response object of Rail incidents API of wmata
 * 
 * @author manohar
 *
 */
public class RespIncidents {
	
	/**
	 * List of incidents
	 */
	@SerializedName("Incidents")
	private List<Incident> incidents;

	public List<Incident> getIncidents() {
		return incidents;
	}

	public void setIncidents(List<Incident> incidents) {
		this.incidents = incidents;
	}
	
	
}
