package com.metrotransit.us.dc.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;

public class FragmentHome extends Fragment implements android.view.View.OnClickListener{
	
	private static final String TAG = "FragmentHome";
	
	private OnClickListener listener;
	
	// declare views
	private TextView nextTrain;
	private TextView nextBus;
	// private TextView nearestStation; private TextView route;
	private TextView railIncidents;
	/*private TextView tripPlanner;*/
	
	@Override
	public void onAttach(Activity activity){
		Log.d(TAG, "onAttach");
		super.onAttach(activity);
		if(activity instanceof OnClickListener){
			listener = (OnClickListener)activity;
		}else{
			throw new ClassCastException(activity.toString() + " must implement FragmentHome.OnClickListener");
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getActivity().getApplication()).enableStrictMode();
		super.onCreate(savedInstanceState);
		/*setRetainInstance(true);*/
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		View view = inflater.inflate(R.layout.fragment_home, container, false);
		// Register views
		nextTrain = (TextView) view.findViewById(R.id.textViewNextTrain);
		nextBus = (TextView) view.findViewById(R.id.textViewNextBus);
		// nearestStation = (TextView) findViewById(R.id.textViewNearestStation); route = (TextView) findViewById(R.id.textViewRoute);
		railIncidents = (TextView) view.findViewById(R.id.textViewRailIncidents);
		/*tripPlanner = (TextView) view.findViewById(R.id.tv_trip_planner);*/

		// set listeners for all views
		nextTrain.setOnClickListener(this);
		nextBus.setOnClickListener(this);
		// nearestStation.setOnClickListener(this); route.setOnClickListener(this);
		railIncidents.setOnClickListener(this);
		/*tripPlanner.setOnClickListener(this);*/
		// Register common receivers of App

		return view;

	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		Log.d(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}
	
	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroyView(){
		Log.d(TAG, "onDestroyView");
		super.onDestroyView();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
	
	@Override
	public void onDetach(){
		Log.d(TAG, "onDetach");
		super.onDetach();
	}
	
	public interface OnClickListener{
		public void onClick(View v);
	}
	
	public void onClick(View v) {
		Log.d(TAG, "onClick");
		listener.onClick(v);
	}
	
}
