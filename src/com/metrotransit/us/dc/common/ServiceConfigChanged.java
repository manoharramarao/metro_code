package com.metrotransit.us.dc.common;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.metrotransit.ApplicationMetro;

public class ServiceConfigChanged extends IntentService {

	private static final String TAG = "ServiceBootComplete";

	/**
	 * 
	 */
	public ServiceConfigChanged() {
		super("ServiceBootComplete");
		Log.d(TAG, "ServiceBootComplete");
	}

	/**
	 * 
	 * @param _name
	 */
	public ServiceConfigChanged(String _name) {
		super(_name);
		Log.d(TAG, "ServiceBootComplete(_name)");
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate();
	}

	@Override
	protected void onHandleIntent(Intent _intent) {
		Log.d(TAG, "onHandleIntent");
		// refresh widgets
		ApplicationMetro appMetro = (ApplicationMetro) getApplication();
		appMetro.refreshWidgets();
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
}
