package com.metrotransit.us.dc.vo;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class RespRailStations {
	
	@SerializedName("Stations")
	private List<Station> stations;

	public List<Station> getStations() {
		return stations;
	}

	public void setStations(List<Station> stations) {
		this.stations = stations;
	}

}
