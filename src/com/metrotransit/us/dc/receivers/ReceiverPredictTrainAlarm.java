package com.metrotransit.us.dc.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

import com.metrotransit.R;
import com.metrotransit.us.dc.providers.ProviderWidgetDetails;
import com.metrotransit.us.dc.services.ServiceTrainPrediction;
import com.metrotransit.us.dc.tables.WidgetDetails;

public class ReceiverPredictTrainAlarm extends BroadcastReceiver{

	public static final String ACTION_REFRESH_TRAIN_PREDICTION_ALARM = "com.metrotransit.us.dc.ACTION_REFRESH_TRAIN_PREDICTION_ALARM";
	private static final String TAG = "ReceiverPredictTrainAlarm";
	
	public ReceiverPredictTrainAlarm(){
		super();
	}
	
	@Override
	public void onReceive(Context _context, Intent _intent) {
		Log.d(TAG, "onReceive");
		if (((PowerManager) _context.getSystemService(Context.POWER_SERVICE)).isScreenOn()){
			Bundle extras = _intent.getExtras();
			int updateFreq = extras.getInt(_context.getResources().getString(R.string.request_code));
			/*int[] appWidgetIds = extras.getIntArray(AppWidgetManager.EXTRA_APPWIDGET_IDS);*/
			// 1. Get all the widgets that have same refresh frequency and auto update is checked
			// 2. fill appWidgetIds array with correct values
			int[] appWidgetIds = null;
			String selection = WidgetDetails.COLUMN_REFRESH_FREQ + "=" + updateFreq + " and " + WidgetDetails.COLUMN_AUTO_REFRESH + "=" + 1;
			Cursor cursor = _context.getContentResolver().query(ProviderWidgetDetails.CONTENT_URI,null,selection, null, null);
			int count = 0;
			if (cursor != null && cursor.getCount() > 0) {
				appWidgetIds = new int[cursor.getCount()];
				cursor.moveToFirst();
				do {
					appWidgetIds[count++] = cursor.getInt(cursor.getColumnIndex(WidgetDetails.COLUMN_WIDGET_ID));
					;
				} while (cursor.moveToNext());
			}else{
				// cancel alarm because there is no widget to be refreshed at this frequency
				AlarmManager alarmManager = (AlarmManager) _context.getSystemService(Context.ALARM_SERVICE);
				String ALARM_ACTION = ReceiverPredictTrainAlarm.ACTION_REFRESH_TRAIN_PREDICTION_ALARM;
				Intent intentToFire = new Intent(ALARM_ACTION);
				PendingIntent alarmIntent = PendingIntent.getBroadcast(_context, updateFreq, intentToFire, 0);
				alarmManager.cancel(alarmIntent);
			}
			if(appWidgetIds != null && appWidgetIds.length > 0){
				Intent intent = new Intent(_context, ServiceTrainPrediction.class);
				intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
				_context.startService(intent);
			}
		}
	}
}
