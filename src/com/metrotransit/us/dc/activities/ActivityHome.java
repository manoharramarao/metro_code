package com.metrotransit.us.dc.activities;

import android.app.Activity;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.metrotransit.ApplicationMetro;
import com.metrotransit.R;
import com.metrotransit.us.dc.fragments.FragmentBusPrediction;
import com.metrotransit.us.dc.fragments.FragmentBusPrediction.BusPrediction;
import com.metrotransit.us.dc.fragments.FragmentHome;
import com.metrotransit.us.dc.fragments.FragmentHome.OnClickListener;
import com.metrotransit.us.dc.fragments.FragmentRailIncidents;
import com.metrotransit.us.dc.fragments.FragmentTrainPrediction;
import com.metrotransit.us.dc.fragments.FragmentTrainPrediction.TrainPrediction;
import com.metrotransit.us.dc.services.ServicePopulateDB;
import android.support.v4.widget.DrawerLayout;

/**
 * Launcher activity for US DC Metro application. 
 * @author manohar
 *
 */
public class ActivityHome extends FragmentActivity implements OnClickListener, TrainPrediction, BusPrediction {

	Bundle savedInstanceState;
	private static final String TAG = "ActivityHome";

    private static final String FRAGMENT_TAG = "com.metrotransit.us.dc.activityhome.currentFragment";

	private FragmentTrainPrediction fragmentTrainPrediction;
	private FragmentBusPrediction fragmentBusPrediction;
	private FragmentRailIncidents fragmentRailIncidents;

    Fragment fragment;

	// I give you the current tab user is on. example next train, next bus, rail incidents and so on. Later convert me to enum from string
	private String currentOperation;
	private String stationName;
	private String stopName;
	private boolean isDualPane = false;

    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;

    @Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_layout);
		this.savedInstanceState = savedInstanceState;
		
		View mainContentPane = findViewById(R.id.home_content);
        isDualPane = mainContentPane != null && mainContentPane.getVisibility() == View.VISIBLE;

        if(!isDualPane){
            // register drawer layout and drawer views
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawerList = (ListView) findViewById(R.id.left_drawer);

            // since list view is used, set the adapter and list itme click listener
            drawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, getResources().getStringArray(R.array.metro_nav_array)));
            drawerList.setOnItemClickListener(new DrawerItemClickListener());

            // Decide what needs to be done apart from showing and hiding the drawer upon opeining and closing of the drawer
            drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close){

                public void onDrawerClosed(View view){
                    getActionBar().setTitle("Drawer Closed");
                    invalidateOptionsMenu();
                }

                public void onDrawerOpened(View view){
                    getActionBar().setTitle("Drawer Opened");
                    invalidateOptionsMenu();
                }

            };

            // set the drawer open/close listener
            drawerLayout.setDrawerListener(drawerToggle);

            // when first time activity is started, load main fragment
            if(savedInstanceState == null){
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.content_frame, new FragmentHome(), FRAGMENT_TAG)
                        .commit();
            }else{
                /*
                fragmentTrainPrediction = (FragmentTrainPrediction)getSupportFragmentManager().findFragmentByTag(FragmentTrainPrediction.TAG);
                fragmentTransaction.replace(R.id.home_content, fragmentTrainPrediction,FragmentTrainPrediction.TAG);
                fragmentTransaction.addToBackStack("ListOfOperations");
                fragmentTransaction.commit();*/
               fragment = (Fragment) getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
            }

        }
        
        /*fragmentHome = (FragmentHome)getFragmentManager().findFragmentById(R.id.fragment_home);*/
        
        // Register all common receivers of App
        ((ApplicationMetro)getApplication()).registerReceivers();
     	
		// TODO find a way if this can be done while installing the application
		// populate entire metro DB - initial data setup
		Intent intent = new Intent(getApplicationContext(), ServicePopulateDB.class);
		getApplicationContext().startService(intent);
		/*File dbFile = getDatabasePath(MetroDBHelper.DB_NAME);
		Log.d(TAG, "DB created successfuly at " + dbFile.getAbsolutePath());*/

	}
	
	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		Log.d(TAG, "onClick");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		if(R.id.textViewNextTrain == ((TextView)v).getId()){
			Log.d(TAG, "clicked on next train");
			if(isDualPane){
				// TODO create Enums at application level for list of operations. Make use of getResources.getString
				currentOperation = "nextTrain";
				if(getSupportFragmentManager().findFragmentByTag(FragmentTrainPrediction.TAG) == null){// first time you are tapping on next train
					fragmentTrainPrediction = new FragmentTrainPrediction();
                    fragmentTransaction.replace(R.id.home_content, fragmentTrainPrediction,FragmentTrainPrediction.TAG);
					fragmentTransaction.addToBackStack("ListOfOperations");
					fragmentTransaction.commit();
				}else{
					fragmentTrainPrediction = (FragmentTrainPrediction)getSupportFragmentManager().findFragmentByTag(FragmentTrainPrediction.TAG);
					fragmentTransaction.replace(R.id.home_content, fragmentTrainPrediction,FragmentTrainPrediction.TAG);
					fragmentTransaction.addToBackStack("ListOfOperations");
					fragmentTransaction.commit();
				}
			}else{
				// start new activity for small devices
				startActivity(new Intent(this, ActivityTrainPrediction.class));
                /*getSupportFragmentManager().beginTransaction()
                        .add(R.id.content_frame, new FragmentTrainPrediction())
                        .commit();*/
			}
		}else if(R.id.textViewNextBus == ((TextView)v).getId()){
			Log.d(TAG, "clicked on next bus");
			if(isDualPane){
				currentOperation = "nextBus";
				if(getSupportFragmentManager().findFragmentByTag(FragmentBusPrediction.TAG) == null){// first time you are tapping on next bus
					fragmentBusPrediction = new FragmentBusPrediction();
					fragmentTransaction.replace(R.id.home_content, fragmentBusPrediction, FragmentBusPrediction.TAG);
					fragmentTransaction.addToBackStack("ListOfOperations");
					fragmentTransaction.commit();
				}else{
					fragmentBusPrediction = (FragmentBusPrediction)getSupportFragmentManager().findFragmentByTag(FragmentBusPrediction.TAG);
					fragmentTransaction.replace(R.id.home_content, fragmentBusPrediction, FragmentBusPrediction.TAG);
					fragmentTransaction.addToBackStack("ListOfOperations");
					fragmentTransaction.commit();
				}
				
			}else{
				startActivity(new Intent(this, ActivityBusPrediction.class));
			}
		}else if(R.id.textViewRailIncidents == ((TextView)v).getId()){
			Log.d(TAG, "clicked on rail incidents");
			if(isDualPane){
				currentOperation = "railIncidents";
				if(getSupportFragmentManager().findFragmentByTag(FragmentRailIncidents.TAG) == null){// first time you are tapping on rail incidents
					fragmentRailIncidents = new FragmentRailIncidents();
					fragmentTransaction.replace(R.id.home_content, fragmentRailIncidents, FragmentRailIncidents.TAG);
					fragmentTransaction.addToBackStack("ListOfOperations");
					fragmentTransaction.commit();
				}else{
					fragmentRailIncidents = (FragmentRailIncidents)getSupportFragmentManager().findFragmentByTag(FragmentRailIncidents.TAG);
					fragmentTransaction.replace(R.id.home_content, fragmentRailIncidents, FragmentRailIncidents.TAG);
					fragmentTransaction.addToBackStack("ListOfOperations");
					fragmentTransaction.commit();
				}
			}else{
				startActivity(new Intent(this,ActivityRailIncidents.class));
			}
		}
	}

	@Override
	public void onStationSelected(String stationName) {
		Log.d(TAG, "onStationSelected");
		this.stationName = stationName;
        ((FragmentTrainPrediction) fragment).updateStationInfo(stationName);
	}
	
	@Override
	public void onStopSelected(String stopName){
		Log.d(TAG, "onStopSelected");
		this.stopName = stopName;
        ((FragmentBusPrediction) fragment).updateStopInfo(stopName);
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState");
		outState.putString("currentOperation", currentOperation);
		if(TextUtils.isEmpty(currentOperation)){
			// do nothing. bail out. There is nothing to save
			// as soon as you launched the app, you are changing orientation. So current operation is nothing. So bail out
		}else {
			if(currentOperation.equalsIgnoreCase("nextTrain")){
                getSupportFragmentManager().putFragment(outState,FragmentTrainPrediction.TAG,fragmentTrainPrediction);
				outState.putString("stationName", stationName);
			}else if(currentOperation.equalsIgnoreCase("nextBus")){
                getSupportFragmentManager().putFragment(outState,FragmentBusPrediction.TAG,fragmentBusPrediction);
				outState.putString("stopName", stopName);
			}
		}
		super.onSaveInstanceState(outState);
    }
	
	@Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
        String savedCurrentOperation = savedInstanceState.getString("currentOperation");
        if(savedCurrentOperation != null){
        	if(savedCurrentOperation.equalsIgnoreCase("nextTrain")){
        		fragmentTrainPrediction = (FragmentTrainPrediction) getSupportFragmentManager().getFragment(savedInstanceState, FragmentTrainPrediction.TAG);
        		fragmentTrainPrediction.updateStationInfo(savedInstanceState.getString("stationName"));
        		currentOperation = savedCurrentOperation;
        		stationName = savedInstanceState.getString("stationName");
        	}else if(savedCurrentOperation.equalsIgnoreCase("nextBus")){
        		fragmentBusPrediction = (FragmentBusPrediction) getSupportFragmentManager().getFragment(savedInstanceState, FragmentBusPrediction.TAG);
        		fragmentBusPrediction.updateStopInfo(savedInstanceState.getString("stopName"));
        		currentOperation = savedCurrentOperation;
        		stopName = savedInstanceState.getString("stopName");
        	}
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        private final String TAG = DrawerItemClickListener.class.getName();

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "onItemClick");
            selectItem(position);
        }

        private void selectItem(int position){
            Log.d(TAG, "selectItem");
            Log.d(TAG, "position selected is " + position);

            switch (position){
                case 0:
                    fragment = new FragmentTrainPrediction();
                    break;
                case 1:
                    fragment = new FragmentBusPrediction();
                    break;
                case 2:
                    fragment = new FragmentRailIncidents();
                    break;
                default:
                    fragment = new FragmentHome();
            }

            Bundle args = new Bundle();
            args.putInt("loading nav2", position);
            fragment.setArguments(args);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment, FRAGMENT_TAG).commit();

            setTitle(Integer.toString(position )+ " frame");
            drawerLayout.closeDrawer(drawerList);

        }
    }

}
