package com.metrotransit.us.dc.appwidget;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.activities.ActivityHome;
import com.metrotransit.us.dc.common.MetroDBHelper;
import com.metrotransit.us.dc.providers.ProviderPredictedTrains;
import com.metrotransit.us.dc.providers.ProviderWidgetDetails;
import com.metrotransit.us.dc.receivers.ReceiverPredictTrainAlarm;
import com.metrotransit.us.dc.services.ServicePopulateDB;
import com.metrotransit.us.dc.services.ServiceTrainPrediction;
import com.metrotransit.us.dc.tables.USDCTrain;
import com.metrotransit.us.dc.tables.WidgetDetails;

/**
 * I am the application widget provider. I provide widget with the latest data in the DB. To know who is updating DB,
 * refer to {@link ServiceTrainPrediction}
 * 
 * @author manohar
 * 
 */
public class PredictTrainsListWidget extends AppWidgetProvider{
	private static final String TAG = "PredictTrainsListWidget";
	public static final String MANUAL_REFRESH_ACTION = "com.metrotransit.us.dc.appwidget.MANUAL_REFRESH_ACTION";
	public static final String WIDGET_PREFERENCES = "com.metrotransit.us.dc.appwidget.WIDGET_PREFERENCES";
	
	public static void updateAppWidget(Context _context, AppWidgetManager appWidgetManager, int appWidgetId){
		Log.d(TAG, "updateAppWidget");
		Intent intent = new Intent(_context, ServiceUpdateWidget.class);
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
		_context.startService(intent);
	}
	
	@Override
	public void onUpdate(Context _context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		
		Log.d(TAG, "onUpdate");
		for(int appWidgetId : appWidgetIds){
			updateAppWidget(_context, appWidgetManager, appWidgetId);
		}
		super.onUpdate(_context, appWidgetManager, appWidgetIds);
	}
	
	@Override
	public void onReceive(Context _context, Intent _intent){
		Log.d(TAG, "onReceive");
		super.onReceive(_context, _intent);
		if(_intent.getAction().equals(MANUAL_REFRESH_ACTION)){
			
			Log.d(TAG, "Refresh button pressed");
			Bundle extras = _intent.getExtras();
			// we got to convert into array because ServiceTrainPrediction service is doing getIntArray
			int[] appWidgetIds = new int[1];
			appWidgetIds[0] = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);
			// start Train prediction service
			Intent intentPredictTrain = new Intent(_context, ServiceTrainPrediction.class);
			intentPredictTrain.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
			_context.startService(intentPredictTrain);
		}
	}
	
	// called everytime app widget is deleted from the app widget host.
	@Override
	public void onDeleted(Context _context, int[] appWidgetIds) {
		Log.d(TAG, "onDeleted");
		super.onDeleted(_context, appWidgetIds);
		
		// delete the DB entry for this widget from widget_details table
		Log.d(TAG, "deleting DB entry for app widget " + appWidgetIds[0] + "from widget_details");
		_context.getContentResolver().delete(ProviderWidgetDetails.CONTENT_URI, WidgetDetails.COLUMN_WIDGET_ID + "=" + appWidgetIds[0], null);
		
		Log.d(TAG, "truncating table dc_us_train");
		_context.getContentResolver().delete(ProviderPredictedTrains.CONTENT_URI, USDCTrain.COLUMN_APP_WIDGET_ID + "=" + appWidgetIds[0], null);
		
		// no need of cancelling alarm here, in the next call to ReceiverPredictTrain receiver, if there is no other
		// widget to be refreshed at the same frequency, then alarm will be canceled
		/*Log.d(TAG, "cancelling alarm for widget " + appWidgetIds[0]);
		AlarmManager alarmManager = (AlarmManager) _context.getSystemService(Context.ALARM_SERVICE);
		String ALARM_ACTION = ReceiverPredictTrainAlarm.ACTION_REFRESH_TRAIN_PREDICTION_ALARM;
		Intent intentToFire = new Intent(ALARM_ACTION);
		PendingIntent alarmIntent = PendingIntent.getBroadcast(_context, appWidgetIds[0], intentToFire, 0);
		alarmManager.cancel(alarmIntent);*/
	}
	
	// called when an instance of the app widget is created for the first time.
	// If user adds two instances of the app widget, this is only called the
	// first time.
	@Override
	public void onEnabled(Context _context){
		Log.d(TAG, "onEnabled");
		super.onEnabled(_context);
		// Could not call service here. Calling service and trying to populate DB in a different thread actually did not
		// work. it started throwing DB malformed. So directly calling.
		MetroDBHelper dbHelper = new MetroDBHelper(_context);
		dbHelper.createDB();
	}
	
	// called when the last instance of the app widget is deleted. Clean up DB
	// and stop the service.
	@Override
	public void onDisabled(Context _context){
		Log.d(TAG, "onDisabled");
		super.onDisabled(_context);
		
		Log.d(TAG, "Last widget deleted - so canceling alarms for all widgets");
		
		// you will have to do this only if there are rows in widget_details table but no widget is on screen which is
		// impossible. But to be on safer side we are doing this
		
		Cursor cursor = _context.getContentResolver().query(ProviderWidgetDetails.CONTENT_URI, null, null, null, null);
		if(null != cursor & cursor.getCount()>0){
			AlarmManager alarmManager = (AlarmManager) _context.getSystemService(Context.ALARM_SERVICE);
			String ALARM_ACTION = ReceiverPredictTrainAlarm.ACTION_REFRESH_TRAIN_PREDICTION_ALARM;
			Intent intentToFire = new Intent(ALARM_ACTION);
			cursor.moveToFirst();
			do{
				int updateFreq = cursor.getInt(cursor.getColumnIndex(WidgetDetails.COLUMN_REFRESH_FREQ));
				PendingIntent alarmIntent = PendingIntent.getBroadcast(_context, updateFreq, intentToFire, 0);
				alarmManager.cancel(alarmIntent);
			}while(cursor.moveToNext());
		}
	}
	
	public static class ServiceUpdateWidget extends IntentService{

		private static final String TAG = "ServiceUpdateWidget";
		public ServiceUpdateWidget() {
			super("ServiceUpdateWidget");
		}
		
		public ServiceUpdateWidget(String name) {
			super(name);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected void onHandleIntent(Intent intent) {
			
			Log.d(TAG, "onHandleIntent");
			// register config change receiver if it is not registered
			ApplicationMetro appMetro = (ApplicationMetro) getApplication();
			appMetro.registerReceivers();
			
			// update widget
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
			int appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
			updateAppWidget(this, appWidgetManager, appWidgetId);
		}
		
		private void updateAppWidget(Context _context, AppWidgetManager appWidgetManager,
	            int appWidgetId) {
			Log.d(TAG, "updateAppWidget");
			Log.d(TAG, "app widget id is " + appWidgetId);
			String stationName = null;
			
			Cursor cursor = getContentResolver().query(ProviderWidgetDetails.CONTENT_URI, null,
					WidgetDetails.COLUMN_WIDGET_ID + " = " + appWidgetId, null, null);

			if (cursor != null && cursor.getCount() > 0) {
				cursor.moveToFirst();
				stationName = cursor.getString(cursor.getColumnIndex(WidgetDetails.COLUMN_STATION));
			}
			
			// set up intent to start remote view service
			Intent intent = new Intent(_context, RemoteViewServicePredictTrain.class);
					
			// Add app widgetID to intent's extras
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			intent.setData(Uri.fromParts("content", String.valueOf(appWidgetId), null));
					
			// instantiate the remote views object for the app widget layout
			RemoteViews views = new RemoteViews(_context.getPackageName(), R.layout.appwidget_layout_train);
			
			// update heading
			views.setTextViewText(R.id.text_view_heading, stationName);
			
			// set up the RemoteViews object to use a RemoteViews adapter
			views.setRemoteAdapter(R.id.list_view_trains, intent);
			
			// empty view is displayed when the collection has no items
			views.setEmptyView(R.id.list_view_trains, R.id.text_view_empty_view);
			
			// set action for manual refresh
			Intent widgetIntent = new Intent(_context,PredictTrainsListWidget.class);
			widgetIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			widgetIntent.setAction(MANUAL_REFRESH_ACTION);
			PendingIntent pendingIntentForButtonRefresh = PendingIntent.getBroadcast(_context, appWidgetId, widgetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			views.setOnClickPendingIntent(R.id.button_refresh, pendingIntentForButtonRefresh);
			
			// set action for perferences button
			Intent widgetPreferencesIntent = new Intent(_context, ActivityPreferences.class);
			widgetPreferencesIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			PendingIntent pendingIntentForPrefButton = PendingIntent.getActivity(_context, appWidgetId, widgetPreferencesIntent,PendingIntent.FLAG_UPDATE_CURRENT);
			views.setOnClickPendingIntent(R.id.button_preferences, pendingIntentForPrefButton);
			
			// create pending intent for interactivity
			Intent templateIntent = new Intent(_context, ActivityHome.class);
			templateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			PendingIntent templatePendingIntent = PendingIntent.getActivity(_context, appWidgetId, templateIntent,PendingIntent.FLAG_UPDATE_CURRENT);
			
			views.setPendingIntentTemplate(R.id.list_view_trains, templatePendingIntent);
			
			// notify the app widget manager to update the widget using the modified remote view
			Log.d(TAG, "just before calling updateAppWidget " + appWidgetId);
			appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}
}
