package com.metrotransit.us.dc.appwidget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.activities.ActivityHome;
import com.metrotransit.us.dc.common.MetroEula;
import com.metrotransit.us.dc.providers.ProviderWidgetDetails;
import com.metrotransit.us.dc.receivers.ReceiverPredictTrainAlarm;
import com.metrotransit.us.dc.services.ServicePopulateDB;
import com.metrotransit.us.dc.services.ServiceTrainPrediction;
import com.metrotransit.us.dc.tables.WidgetDetails;

/**
 * Preferences for widgets. I am called when user tries to add widget
 * 
 * @author manohar
 * 
 */
public class ActivityPreferences extends PreferenceActivity {

	private static final String TAG = "ActivityPreferences";

	public static final String PREF_AUTO_UPDATE = "PREF_AUTO_UPDATE";
	public static final String PREF_REFRESH_FREQ = "PREF_UPDATE_FREQ";
	public static final String PREF_STATION_NAME = "PREF_STATION_NAME";
	

	@Override
	public void onCreate(Bundle _savedInstanceState) {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate(_savedInstanceState);
		getFragmentManager().beginTransaction().replace(android.R.id.content, new MetroPreferencesFragment(), MetroPreferencesFragment.TAG).commit();
		/*addPreferencesFromResource(R.xml.user_preferences);*/
	}
	
	public static class MetroPreferencesFragment extends PreferenceFragment{
		public static final String TAG = "MetroPreferencesFragment";
		
		private TextView textViewSave;
		private TextView textViewCancel;
		private AlarmManager alarmManager;
		private PendingIntent alarmIntent;
		
		// variables to store the preferences when activity started
		boolean prefAutoUpdate;
		String prefRefreshFreq;
		String prefStationName;

		private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

		SharedPreferences prefs;
		
		@Override
		public void onCreate(final Bundle savedInstanceState){
			Log.d(TAG, "onCreate");
			((ApplicationMetro)getActivity().getApplication()).enableStrictMode();
			new MetroEula(getActivity(), null).show();
			super.onCreate(savedInstanceState);
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
			Log.d(TAG, "onCreateView");
			super.onCreateView(inflater, container, savedInstanceState);
			
			// before you inflate the views, update preferences with widget details from DB
			// save app widget ID.
			Intent intent = getActivity().getIntent();
			Bundle extras = intent.getExtras();
			if (extras != null) {
				appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
			}
			Log.d(TAG, "appwidget ID is " + appWidgetId);
			// If they gave us an intent without the widget id, just bail out.
			if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
				getActivity().finish();
			}

			// load preferences with widget details pulling from DB. Remember, preferences is just used to show on
			// screen. Actual details are in DB
			prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			SharedPreferences.Editor prefsEditor = prefs.edit();
			Cursor cursor = getActivity().getContentResolver().query(ProviderWidgetDetails.CONTENT_URI, null,
					WidgetDetails.COLUMN_WIDGET_ID + "=" + appWidgetId, null, null);
			if (null != cursor && cursor.getCount() > 0) {
				cursor.moveToFirst();
				prefsEditor.putString(getResources().getString(R.string.pref_station_name),
						cursor.getString(cursor.getColumnIndex(WidgetDetails.COLUMN_STATION)));
				prefsEditor.putBoolean(getResources().getString(R.string.pref_auto_update),
						cursor.getString(cursor.getColumnIndex(WidgetDetails.COLUMN_AUTO_REFRESH))
								.equalsIgnoreCase("1") ? true : false);
				prefsEditor.putString(getResources().getString(R.string.pref_refresh_freq),
						cursor.getString(cursor.getColumnIndex(WidgetDetails.COLUMN_REFRESH_FREQ)));
				prefsEditor.apply();
			}
			
			View view = inflater.inflate(R.layout.app_widget_buttons, container, false);
			addPreferencesFromResource(R.xml.user_preferences);
			return view;
		}
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState){
			
			Log.d(TAG, "onActivityCreated");
			super.onActivityCreated(savedInstanceState);

			// save button
			textViewSave = (TextView) getActivity().findViewById(R.id.text_view_save);
			textViewSave.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					onSave();
				}
			});

			// cancel button
			textViewCancel = (TextView) getActivity().findViewById(R.id.text_view_cancel);
			textViewCancel.setOnClickListener(new OnClickListener() {
				
				public void onClick(View v) {
					onCancel();
				}
			});
		}
		
		/**
		 * <li>update DB with widget details from preferences</li>
		 * <li>set alarm based on updateFreq. This is to maintain alarms per update freq rather than having alarms per widget</li>
		 * <li>start train prediction</li>
		 * <li>update widget</li>
		 * <li>pass back original app widget id</li>
		 */
		protected void onSave() {
			Log.d(TAG, "onSave");

			boolean isAutoUpdateEnabled = prefs.getBoolean(ActivityPreferences.PREF_AUTO_UPDATE, false);
			int updateFreq = Integer.parseInt(prefs.getString(ActivityPreferences.PREF_REFRESH_FREQ, "60"));
			
			// get values from preferences and update the DB
			// create content values
			ContentValues values = new ContentValues();
			values.put(WidgetDetails.COLUMN_STATION, prefs.getString(ActivityPreferences.PREF_STATION_NAME, getResources().getString(R.string.default_station_name)));
			values.put(WidgetDetails.COLUMN_AUTO_REFRESH, isAutoUpdateEnabled? 1 : 0);
			values.put(WidgetDetails.COLUMN_REFRESH_FREQ, updateFreq);
			values.put(WidgetDetails.COLUMN_WIDGET_ID, appWidgetId);
			
			ContentResolver cr = getActivity().getContentResolver(); 
			Cursor cursor = cr.query(ProviderWidgetDetails.CONTENT_URI, null, WidgetDetails.COLUMN_WIDGET_ID + "=" + appWidgetId, null, null);
			if(cursor != null && cursor.getCount() > 0){
				cursor.moveToFirst();
				cr.update(ProviderWidgetDetails.CONTENT_URI, values, WidgetDetails.COLUMN_WIDGET_ID + "=" + appWidgetId, null);
				cursor.close();
			}else{
				cr.insert(ProviderWidgetDetails.CONTENT_URI, values);
				cursor.close();
			}
			
			// set the alarm if auto update is checked
			if (alarmManager == null || alarmIntent == null) {
				alarmManager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
				String ALARM_ACTION = ReceiverPredictTrainAlarm.ACTION_REFRESH_TRAIN_PREDICTION_ALARM;
				Intent intentToFire = new Intent(ALARM_ACTION);
				intentToFire.putExtra(getResources().getString(R.string.request_code), updateFreq);
				alarmIntent = PendingIntent.getBroadcast(getActivity().getApplicationContext(), updateFreq, intentToFire, 0);
				int alarmType = AlarmManager.ELAPSED_REALTIME;
				long timeToRefresh = SystemClock.elapsedRealtime() + updateFreq * 60 * 1000;
				// cancel the previous alarms if set
				alarmManager.cancel(alarmIntent);
				alarmManager.setInexactRepeating(alarmType, timeToRefresh, updateFreq * 60 * 1000, alarmIntent);
			}
			
			Intent intentPredictTrain = new Intent(getActivity().getApplicationContext(), ServiceTrainPrediction.class);
			int[] appWidgetIds = new int[1];
			appWidgetIds[0] = appWidgetId;
			intentPredictTrain.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetIds);
			getActivity().getApplicationContext().startService(intentPredictTrain);
			AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getActivity().getApplicationContext());
			// TODO understand why notifying app widget at this point is not working and you had to call updateappwidget explicitly.
			appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,R.id.list_view_trains);
			/*appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetId,R.id.text_view_heading);*/
			PredictTrainsListWidget.updateAppWidget(getActivity().getApplicationContext(), appWidgetManager, appWidgetId);
			// TODO Am I really required here?
			// Make sure we pass back the original appWidgetId
			Intent resultValue = new Intent();
			resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			getActivity().setResult(RESULT_OK, resultValue);
			getActivity().finish();
			
		}

		protected void onCancel() {
			Log.d(TAG, "onCancel");
			getActivity().finish();
		}

		public void onBackPressed() {
			Log.d(TAG, "onBackPressed");
			getActivity().finish();
		}
		
		@Override
		public void onPause(){
			Log.d(TAG, "onPause");
			super.onPause();
		}
	}
}
