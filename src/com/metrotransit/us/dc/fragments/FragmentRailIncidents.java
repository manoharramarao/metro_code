package com.metrotransit.us.dc.fragments;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.adapters.AdapterRailIncidents;
import com.metrotransit.us.dc.core.USDCMetroClient;
import com.metrotransit.us.dc.core.USDCMetroClientImpl;
import com.metrotransit.us.dc.vo.Incident;
import com.metrotransit.us.dc.vo.RespIncidents;

public class FragmentRailIncidents extends Fragment {
	
	public static final String TAG = "FragmentRailIncidents";
	
	private ListView listViewIncidents;
	private ProgressBar progressBar;
	private TextView emptyView;
	List<Incident> incidents;
	private AdapterRailIncidents adapterRailIncidents;
	
	private String result;
	
	@Override
	public void onAttach(Activity activity){
		Log.d(TAG, "onAttach");
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getActivity().getApplication()).enableStrictMode();
		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
		/*setRetainInstance(true);*/
	}
	
	@Override
	public void onCreateOptionsMenu (Menu menu, MenuInflater inflater){
		Log.d(TAG, "onCreateOptionsMenu");
		super.onCreateOptionsMenu(menu, inflater);
	    inflater.inflate(R.menu.refresh_menu, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		Log.d(TAG, "onOptionsItemSelected");
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			Log.d(TAG, "refresh menu button clicked");
			// hide empty view
			emptyView.setVisibility(View.GONE);
			// show progress bar and hide the list
			progressBar.setVisibility(1);
			updateRailIncidents();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.d(TAG, "onCreateView");
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_rail_incidents, container, false);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState){
		Log.d(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
		getActivity().getActionBar().setTitle(getResources().getString(R.string.rail_incidents));
		// register views
		progressBar = (ProgressBar) getActivity().findViewById(R.id.progressBarListIncidents);
		listViewIncidents = (ListView) getActivity().findViewById(R.id.listViewIncidents);
		emptyView = (TextView) getActivity().findViewById(R.id.list_incidents_text_view_empty_view);

		// hide empty view
		emptyView.setVisibility(View.GONE);

		// show progress bar and hide the list
		progressBar.setVisibility(1);
		updateRailIncidents();
	}
	
	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroyView(){
		Log.d(TAG, "onDestroyView");
		super.onDestroyView();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
	
	@Override
	public void onDetach(){
		Log.d(TAG, "onDetach");
		super.onDetach();
	}

	class Incidents extends AsyncTask<String, Integer, String> {
		private final String SUBTAG = Incidents.class.getSimpleName();

		// I do the actvity in background. In this case I get the rail incidents
		@Override
		protected String doInBackground(String... params) {
			
			Log.d(SUBTAG, "doInBachground");
			// I tell you whether web service call was succeeded or failed. If
			// it failed, then I also give you the meaningful sentence
			// explaining the reason.
			result = getResources().getString(R.string.success);
			
			// TODO for God sake don't create me every time when you need one.
			// Keep me at application level. So that for the entire application
			// I will be only one.
			USDCMetroClient dcMetroClient = new USDCMetroClientImpl();
			try {
				RespIncidents respIncidents = dcMetroClient.getRailIncidents(getResources().getString(R.string.api_key));
				incidents = respIncidents.getIncidents();
				if(null == incidents || incidents.isEmpty()){
					result = "No Rail incidents";
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
				result = getResources().getString(R.string.int_conn_fail);
			} catch (IOException e) {
				e.printStackTrace();
				result = getResources().getString(R.string.int_conn_fail);
			}
			return result;
		}

		// I will be called when there is some status to update
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			progressBar.setVisibility(1);
			listViewIncidents.setVisibility(View.GONE);
		}

		// I will be called once the background activity is over
		@Override
		protected void onPostExecute(String result) {
			// After fetching rail incidents, hide progress bar
			progressBar.setVisibility(View.GONE);
			
			// show rail incidents in list view
			paintDetails();
			/*if (result.equalsIgnoreCase("success")) {
				paintDetails();
			} else {
				Toast.makeText(ActivityRailIncidents.this, result,
						Toast.LENGTH_LONG).show();
			}*/
		}
	}
	
	private void paintDetails() {

		// TODO Disable list to have onclick capability
		
		// Paint the info
		if (null != incidents && !incidents.isEmpty()) {
			listViewIncidents.setVisibility(View.VISIBLE);
			adapterRailIncidents = new AdapterRailIncidents(
					getActivity(), incidents);
			listViewIncidents.setAdapter(adapterRailIncidents);
			// get list animation and start animating list
			// TODO change the name of the animation. This is not particular to
			// train info list.
			Animation drawFromBottom = AnimationUtils.loadAnimation(getActivity(),
					R.anim.train_info_list);
			listViewIncidents.startAnimation(drawFromBottom);
		}else{
			emptyView.setText(result);
			emptyView.setVisibility(View.VISIBLE);
			listViewIncidents.setEmptyView(emptyView);
		}
	}
	
	public void updateRailIncidents() {
		// Fetch incidents in background and once done paint listview
		new Incidents().execute();
	}
}
