package com.metrotransit.us.dc.common;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.appwidget.ActivityPreferences;

public class ActivityWidgetLauncher extends Activity{
	private static final String TAG = "ActivityLauncher";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		new MetroEula(this, ActivityPreferences.class).show();
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
}
