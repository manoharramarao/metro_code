package com.metrotransit.us.dc.adapters;

import java.util.List;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.metrotransit.R;
import com.metrotransit.us.dc.adapters.AdapterRailIncidents.RailIncidentsRowView;
import com.metrotransit.us.dc.vo.RespIncidents;

/**
 * Bridget between fragment_rail_incidents and {@link RespIncidents}
 * @author manohar
 *
 */
public class AdapterLinesAffected extends ArrayAdapter{
	
	private final Activity activity;
	private final List lines;
	
	@SuppressWarnings("unchecked")
	public AdapterLinesAffected(Activity activity, List objects){
		super(activity, R.layout.list_lines_affected_row, objects);
		this.activity = activity;
		this.lines = objects;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View rowView = convertView;
		LinesAffectedRowView linesAffectedRowView = null;
		
		if(null == rowView){
			// Get a new instance of the row layout view
			LayoutInflater inflater = activity.getLayoutInflater();
			rowView = inflater.inflate(R.layout.list_lines_affected_row, parent, false);
			
			// Hold the view objects in an object, so they don't need to be
			// re-fetched
			linesAffectedRowView = new LinesAffectedRowView();
			linesAffectedRowView.viewLinesAffected = (View) rowView.findViewById(R.id.viewLinesAffectedColor);
			/*railIncidentsRowView.line = (TextView) rowView.findViewById(R.id.textViewIncidentLine);*/
			
			// Cache the view objects in the tag, so they can be re-accessed
			// later
			rowView.setTag(linesAffectedRowView);
		}else{
			linesAffectedRowView = (LinesAffectedRowView) rowView.getTag();
		}
		
		// Transfer the Train data from the data object to the view objects
		String line = (String) lines.get(position);
		
		if(line.equalsIgnoreCase("RD")){
			linesAffectedRowView.viewLinesAffected.setBackgroundColor(Color.RED);
		}else if(line.equalsIgnoreCase("OR")){
			linesAffectedRowView.viewLinesAffected = (View) rowView
					.findViewById(R.id.viewLinesAffectedColor);
			linesAffectedRowView.viewLinesAffected.setBackgroundColor(activity
					.getResources().getColor(R.color.orangeLine));
		}else if(line.equalsIgnoreCase("BL")){
			linesAffectedRowView.viewLinesAffected.setBackgroundColor(Color.BLUE);
		}else if(line.equalsIgnoreCase("YL")){
			linesAffectedRowView.viewLinesAffected.setBackgroundColor(Color.YELLOW);
		}else if(line.equalsIgnoreCase("GR")){
			linesAffectedRowView.viewLinesAffected.setBackgroundColor(Color.GREEN);
		}
		// TODO Imp. lines affected will be list and not single line
		/*railIncidentsRowView.line.setText(incident.getLinesAffected());*/
		return rowView;
	}

	@Override
	public boolean isEnabled(int position){
		return false;
	}
	
	protected static class LinesAffectedRowView {
		protected View viewLinesAffected;
	}
}
