package com.metrotransit.us.dc.vo;

import com.google.gson.annotations.SerializedName;

public class AIMPredictionTrainInfo {
	
	@SerializedName("Car")
	String numOfCars;
	
	@SerializedName("Destination")
	String destination;
	
	@SerializedName("DestinationCode")
	String destinationCode;
	
	@SerializedName("DestinationName")
	String destinationName;
	
	@SerializedName("Group")
	int group;
	
	@SerializedName("Line")
	String line;
	
	@SerializedName("LocationCode")
	String locationCode;
	
	@SerializedName("LocationName")
	String locationName;
	
	@SerializedName("Min")
	String numOfMins;
	
	public String getNumOfCars() {
		return numOfCars;
	}
	public void setNumOfCars(String numOfCars) {
		this.numOfCars = numOfCars;
	}
	
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDestinationCode() {
		return destinationCode;
	}
	public void setDestinationCode(String destinationCode) {
		this.destinationCode = destinationCode;
	}
	public String getDestinationName() {
		return destinationName;
	}
	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
	public int getGroup() {
		return group;
	}
	public void setGroup(int group) {
		this.group = group;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getNumOfMins() {
		return numOfMins;
	}
	public void setNumOfMins(String numOfMins) {
		this.numOfMins = numOfMins;
	}
	
}
