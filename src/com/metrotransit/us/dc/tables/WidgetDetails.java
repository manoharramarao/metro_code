package com.metrotransit.us.dc.tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class WidgetDetails {
	
	private static final String TAG = "WidgetDetails";
	
	public static final String TABLE_WIDGET_DETAILS = "widget_details";
	
	// column names
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_WIDGET_ID = "widget_id";
	public static final String COLUMN_STATION = "station";
	public static final String COLUMN_REFRESH_FREQ = "refresh_freq";
	public static final String COLUMN_AUTO_REFRESH = "auto_refresh";
	
	private static final String DATABASE_CREATE = "create table " + TABLE_WIDGET_DETAILS + " ("
			+ COLUMN_ID + " integer primary key autoincrement, " 
			+ COLUMN_WIDGET_ID + " integer, "
			+ COLUMN_STATION + " text, "
			+ COLUMN_REFRESH_FREQ + " integer, "
			+ COLUMN_AUTO_REFRESH + " integer);";
	
	/**
	 * 
	 * @param db
	 */
	public static void onCreate(SQLiteDatabase db){
		Log.d(TAG, "onCreate");
		db.execSQL(DATABASE_CREATE);
	}
	
	/**
	 * 
	 * @param db
	 * @param oldVersion
	 * @param newVersion
	 */
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		Log.d(TAG, "onUpgrade");
		Log.w(TAG, "upgrading from version " + oldVersion + " to " + newVersion);
		db.execSQL("drop table if exists " + TABLE_WIDGET_DETAILS);
		onCreate(db);
	}
}
