package com.metrotransit.us.dc.fragments;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.adapters.AdapterStopInfo;
import com.metrotransit.us.dc.core.USDCMetroClient;
import com.metrotransit.us.dc.core.USDCMetroClientImpl;
import com.metrotransit.us.dc.providers.ProviderUSDCStops;
import com.metrotransit.us.dc.tables.USDCStop;
import com.metrotransit.us.dc.vo.BusInfo;
import com.metrotransit.us.dc.vo.RespBusPrediction;

public class FragmentBusPrediction extends Fragment implements OnItemClickListener, OnEditorActionListener, OnClickListener, TextWatcher{
	
	public static final String TAG = "FragmentBusPrediction";
	
	private AutoCompleteTextView atvBusStops;
	private Button bClear;
	private ListView lvStopInfo;
	private TextView emptyView;
	private ProgressBar progressBar;
	
	
	private ApplicationMetro metroApp;
	private SimpleCursorAdapter adapter;
	private USDCMetroClient dcMetroClient;
	
	// To store the result and error string of API call
	private String result;
	private String errorMessage;
	private String requestedBusStation;
	private List<BusInfo> predictedBusInfo;
	private AdapterStopInfo adapterStopInfo;
	private String stopName;
	private BusPrediction busPrediction;
	
	@Override
	public void onAttach(Activity activity){
		Log.d(TAG, "onAttach");
		super.onAttach(activity);
		if(activity instanceof BusPrediction){
			busPrediction = (BusPrediction)activity;
		}else{
			throw new ClassCastException(activity.toString() + " must implement BusPrediction.onStopSelected");
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getActivity().getApplication()).enableStrictMode();
		setHasOptionsMenu(true);
		super.onCreate(savedInstanceState);
		/*setRetainInstance(true);*/
	}
	
	@Override
	public void onCreateOptionsMenu (Menu menu, MenuInflater inflater){
		Log.d(TAG, "onCreateOptionsMenu");
		super.onCreateOptionsMenu(menu, inflater);
	    inflater.inflate(R.menu.refresh_menu, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		Log.d(TAG, "onOptionsItemSelected");
		switch (item.getItemId()) {
		case R.id.menu_refresh:
			Log.d(TAG, "refresh menu button clicked");
			stopName = atvBusStops.getText().toString();
			busPrediction.onStopSelected(stopName);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Log.d(TAG, "onCreateView");
		super.onCreateView(inflater, container, savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_bus_prediction, container, false);
		view.requestFocus();
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
		getActivity().getActionBar().setTitle(getResources().getString(R.string.next_bus));
		
		atvBusStops = (AutoCompleteTextView) getActivity().findViewById(R.id.atv_bus_stops);
		atvBusStops.setOnEditorActionListener(this);
		atvBusStops.addTextChangedListener(this);
		atvBusStops.setOnItemClickListener(this);
		
		progressBar = (ProgressBar) getActivity().findViewById(R.id.pb);
		/*
		 * String[] stations = getResources().getStringArray(R.array.bus_stops); ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_stations, stations);
		 * atvBusStops.setAdapter(adapter); atvBusStops.setOnItemClickListener(this); atvBusStops.setOnEditorActionListener(this);
		 */

		bClear = (Button) getActivity().findViewById(R.id.b_clear);
		bClear.setOnClickListener(this);

		lvStopInfo = (ListView) getActivity().findViewById(R.id.lv_stop_info);
		emptyView = (TextView) getActivity().findViewById(R.id.empty_view);

		metroApp = (ApplicationMetro) getActivity().getApplication();
		
		final int[] to = new int[] { R.id.TextViewStation };
		final String[] from = new String[] { USDCStop.COLUMN_NAME };
		adapter = new SimpleCursorAdapter(getActivity().getApplicationContext(), R.layout.list_stations, null, from, to);
		initAdapter();
		
		if(null != predictedBusInfo){
			paintScreen();
		}else{
			metroApp.showKeyboard();
		}
	}
	
	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroyView(){
		Log.d(TAG, "onDestroyView");
		super.onDestroyView();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}
	
	@Override
	public void onDetach(){
		Log.d(TAG, "onDetach");
		super.onDetach();
	}
	
	private void initAdapter(){
		Log.d(TAG, "initAdapter");
		/*final int[] to = new int[]{R.id.TextViewStation};
		final String[] from = new String[]{USDCStop.COLUMN_NAME};
		adapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.list_stations, null,
				from, to);*/

		adapter.setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
			public CharSequence convertToString(Cursor cursor) {
				/*Log.d(TAG, "convertToString");*/
				final  int colIndex = cursor.getColumnIndexOrThrow(USDCStop.COLUMN_NAME);
				/*Log.d(TAG, "column index is " + colIndex);
				Log.d(TAG, "converted string is " + cursor.getString(colIndex));*/
				return cursor.getString(colIndex);
			}
		});
		
		adapter.setFilterQueryProvider(new FilterQueryProvider() {
			public Cursor runQuery(CharSequence constraint) {
				Log.d(TAG, "runQuery");
				String[] projection = new String[]{USDCStop.COLUMN_ID,USDCStop.COLUMN_NAME};
				StringBuffer selection;
				String sortOrder = USDCStop.COLUMN_NAME;
				if(!TextUtils.isEmpty(atvBusStops.getText().toString())){
					Log.d(TAG, "text is not empty");
					String[] patterns = {atvBusStops.getText().toString()};
					if(atvBusStops.getText().toString().contains(" ")){
						patterns = atvBusStops.getText().toString().split(" ");
					}
					selection = new StringBuffer(USDCStop.COLUMN_NAME);
					int count = 1;
					for(String splitPattern : patterns){
						 selection
						.append(" LIKE '%")
						.append(splitPattern)
						.append("%'");
						if(patterns.length > 0 && count != patterns.length){
							selection
							.append(" and ")
							.append(USDCStop.COLUMN_NAME);
						}
						count++;
					}
				}else{
					Log.d(TAG, "setting selection to null");
					selection = null;
				}
				return getActivity().getContentResolver().query(ProviderUSDCStops.CONTENT_URI, projection,
						(selection == null) ? null : selection.toString(), null, sortOrder);
				/*return managedQuery(ProviderUSDCStops.CONTENT_URI, projection, (selection==null)?null:selection.toString(), null,
						sortOrder);*/
			}
		});
		atvBusStops.setAdapter(adapter);
	}
	
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.d(TAG, "onItemClick");
		metroApp.hideKeyboard();
		stopName = atvBusStops.getText().toString();
		busPrediction.onStopSelected(stopName);
	}
	
	class Bus extends AsyncTask<String, Integer, String> {
		private final String SUBTAG = Bus.class.getSimpleName();

		// I do the actvity of fetching bus info from core components DCmetro
		// in background
		@Override
		protected String doInBackground(String... params) {
			Log.d(SUBTAG, "doInBackground");
			dcMetroClient = new USDCMetroClientImpl();
			result = getResources().getString(R.string.failure);

			if (TextUtils.isEmpty(getRequestedBusStation())) {
				errorMessage = getResources().getString(R.string.enter_stop_name);
			} else {
				
				String[] projection = {USDCStop.COLUMN_ID, USDCStop.COLUMN_STOP_ID};
				String selection = USDCStop.COLUMN_NAME + "=" + "'" + getRequestedBusStation() + "'";
				List<String> busStopIds = new ArrayList<String>();
				// get busStop ID
				Cursor cursor = getActivity().getContentResolver().query(ProviderUSDCStops.CONTENT_URI, projection, selection, null,
						USDCStop.COLUMN_NAME);
				if (cursor != null && cursor.getCount()>0) {
					cursor.moveToFirst();
					do {
						Log.d(SUBTAG, cursor.getString(cursor.getColumnIndexOrThrow(USDCStop.COLUMN_STOP_ID)));
						busStopIds.add(cursor.getString(cursor.getColumnIndexOrThrow(USDCStop.COLUMN_STOP_ID)));
					} while (cursor.moveToNext());
					Gson gson = new Gson();
					RespBusPrediction respBusPrediction = new RespBusPrediction();
					if(metroApp.isConnectedToNetwork()){
						try {
							respBusPrediction = gson.fromJson(dcMetroClient.predictBus(getResources().getString(R.string.api_key), busStopIds), RespBusPrediction.class);
							if(respBusPrediction != null){
								Log.d(SUBTAG,
										"num of predictions"
												+ Integer.toString(respBusPrediction.getPredictions().size()));
								for(BusInfo stopInfo: respBusPrediction.getPredictions()){
									predictedBusInfo.add(stopInfo);
								}
								Collections.sort(predictedBusInfo, new SortBusPredictedInfo());
								result = getResources().getString(R.string.success);
							}
							
						} catch (JsonSyntaxException e) {
							Log.e(SUBTAG, e.getMessage());
							errorMessage = getResources().getString(R.string.fatal_error);
							e.printStackTrace();
						} catch (MalformedURLException e) {
							Log.e(SUBTAG, e.getMessage());
							errorMessage = getResources().getString(R.string.fatal_error);
							e.printStackTrace();
						} catch (IOException e) {
							Log.e(SUBTAG, e.getMessage());
							errorMessage = getResources().getString(R.string.fatal_error);
							e.printStackTrace();
						}
					}else{
						errorMessage = getResources().getString(R.string.int_conn_fail);
					}
				}else{
					errorMessage = getResources().getString(R.string.wrong_stop_name);
				}
			}
			return result;
		}
		
		// I will be called when there is some status to update
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		// I will be called once the background activity is over
		@Override
		protected void onPostExecute(String result) {
			Log.d(SUBTAG, "onPostExecute");
			progressBar.setVisibility(View.GONE);
			paintScreen();
		}
	}

	class SortBusPredictedInfo implements Comparator<BusInfo> {
		
		public int compare(BusInfo lhs, BusInfo rhs) {
			Log.d("SortBusPredictedInfo", "compare");
			return lhs.getRouteId().compareToIgnoreCase(rhs.getRouteId());
		}
	}
	
	private void paintScreen(){
		Log.d(TAG, "paintScreen");
		if(result.equalsIgnoreCase(getResources().getString(R.string.success))){
			if(null != predictedBusInfo && !predictedBusInfo.isEmpty()){
				Log.d(TAG, "predictedBusInfo is neither null not empty");
				emptyView.setVisibility(View.GONE);
				progressBar.setVisibility(View.GONE);
				adapterStopInfo = new AdapterStopInfo(getActivity(), predictedBusInfo);
				lvStopInfo.setAdapter(adapterStopInfo);
				Animation drawFromBottom = AnimationUtils.loadAnimation(getActivity(), R.anim.train_info_list);
				lvStopInfo.startAnimation(drawFromBottom);
			}else{
				emptyView.setText(getResources().getString(R.string.no_buses_avail));
				lvStopInfo.setEmptyView(emptyView);
			}
		}else{
			emptyView.setText(errorMessage);
			lvStopInfo.setEmptyView(emptyView);
		}
		
	}
	
	public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
		Log.d(TAG, "onEditorAction");
		metroApp.hideKeyboard();
		stopName = atvBusStops.getText().toString();
		busPrediction.onStopSelected(stopName);
		return true;
	}

	public void onClick(View v) {
		Log.d(TAG, "onClick");
		atvBusStops.setText("");
		metroApp.showKeyboard();
	}

	public void afterTextChanged(Editable s) {
		Log.d(TAG, "afterTextChanged");
	}

	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		Log.d(TAG, "beforeTextChanged");
		
	}

	public void onTextChanged(CharSequence s, int start, int before, int count) {
		Log.d(TAG, "onTextChanged");
		Log.d(TAG, "Text is " + atvBusStops.getText().toString());
		initAdapter();
	}
	
	public String getRequestedBusStation() {
		return requestedBusStation;
	}

	public void setRequestedBusStation(String requestedBusStation) {
		this.requestedBusStation = requestedBusStation;
	}
	
	public void updateStopInfo(String stopName){
		Log.d(TAG, "updateStopInfo");
		predictedBusInfo = new ArrayList<BusInfo>();
		lvStopInfo.setEmptyView(progressBar);
		emptyView.setVisibility(View.GONE);
		progressBar.setVisibility(View.VISIBLE);
		setRequestedBusStation(atvBusStops.getText().toString().trim());
		lvStopInfo.setAdapter(new AdapterStopInfo(getActivity(),predictedBusInfo));
		new Bus().execute();
	}
	
	public interface BusPrediction{
		public void onStopSelected(String stopName);
	}
}
