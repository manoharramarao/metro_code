package com.metrotransit.us.dc.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.metrotransit.us.dc.common.MetroDBHelper;

public class ServicePopulateDB extends IntentService{

	private static final String TAG = "ServiceBootComplete";
	
	/**
	 * 
	 */
	public ServicePopulateDB(){
		super("ServiceBootComplete");
		Log.d(TAG, "ServiceBootComplete");
	}
	
	/**
	 * 
	 * @param _name
	 */
	public ServicePopulateDB(String _name) {
		super(_name);
		Log.d(TAG, "ServiceBootComplete(_name)");
	}

	@Override
	protected void onHandleIntent(Intent arg0) {
		Log.d(TAG, "onHandleIntent");
		MetroDBHelper dbHelper = new MetroDBHelper(getApplicationContext());
		dbHelper.createDB();
	}

}
