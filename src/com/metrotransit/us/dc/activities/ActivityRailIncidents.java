package com.metrotransit.us.dc.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;

import com.metrotransit.R;
import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.fragments.FragmentRailIncidents;

/**
 * I tell you all the rail incidents for US DC Metro. I call wmata to get all incidents.
 * @author manohar
 *
 */
public class ActivityRailIncidents extends FragmentActivity {

	private static final String TAG = "ActivityRailIncidents";
	private FragmentRailIncidents fragmentRailIncidents;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		((ApplicationMetro)getApplication()).enableStrictMode();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rail_incidents);
		fragmentRailIncidents = (FragmentRailIncidents)getSupportFragmentManager().findFragmentById(R.id.fragment_rail_incidents);
		this.getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
	}

	@Override
	public void onStart(){
		Log.d(TAG, "onStart");
		super.onStart();
	}
	
	@Override
	public void onResume(){
		Log.d(TAG, "onResume");
		super.onResume();
	}
	
	@Override
	public void onPause(){
		Log.d(TAG, "onPause");
		super.onPause();
	}
	
	@Override
	public void onStop(){
		Log.d(TAG, "onStop");
		super.onStop();
	}
	
	@Override
	public void onDestroy(){
		Log.d(TAG, "onDestroy");
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(TAG, "onOptionsItemSelected");

		switch (item.getItemId()) {
		case android.R.id.home:
			Log.d(TAG, "up navigation button clicked");
			Intent parentActivityIntent = new Intent(this, ActivityHome.class);
			parentActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(parentActivityIntent);
			finish();
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
    protected void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState");
		// TODO make sure this is really required
		if(null != fragmentRailIncidents){
			getSupportFragmentManager().putFragment(outState, FragmentRailIncidents.TAG, fragmentRailIncidents);
		}
		super.onSaveInstanceState(outState);
    }
	
	@Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState");
        super.onRestoreInstanceState(savedInstanceState);
        fragmentRailIncidents = (FragmentRailIncidents) getSupportFragmentManager().getFragment(savedInstanceState, FragmentRailIncidents.TAG);
        fragmentRailIncidents.updateRailIncidents();
	}
}
