package com.metrotransit.us.dc.providers;



import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.metrotransit.ApplicationMetro;
import com.metrotransit.us.dc.common.MetroDBHelper;
import com.metrotransit.us.dc.tables.USDCTrain;


/**
 * 
 * @author manohar
 *
 */

public class ProviderPredictedTrains extends ContentProvider{

	private static final String TAG = "ProviderPredictedTrains";
	public static final Uri CONTENT_URI = Uri.parse("content://com.metrotransit.us.dc.predictedtrainsprovider/trains");
	
	MetroDBHelper dbHelper;
	
	private static final int PREDICTED_TRAINS = 1;
	private static final int PREDICTED_TRAIN_ID = 2;
	private static final UriMatcher uriMatcher;
	
	static{
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI("com.metrotransit.us.dc.predictedtrainsprovider", "trains", PREDICTED_TRAINS);
		uriMatcher.addURI("com.metrotransit.us.dc.predictedtrainsprovider", "trains/#", PREDICTED_TRAIN_ID);
	}
	
	
	@Override
	public int delete(Uri _uri, String _where, String[] _whereArgs) {
		Log.d(TAG, "delete");
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int count;
		
		switch (uriMatcher.match(_uri)) {
		case PREDICTED_TRAINS:
			count = db.delete(USDCTrain.TABLE_USDCTRAINS, _where, _whereArgs);
			break;
		case PREDICTED_TRAIN_ID:
			String segment = _uri.getPathSegments().get(1);
			count = db.delete(USDCTrain.TABLE_USDCTRAINS,
					USDCTrain.COLUMN_ID
							+ "="
							+ segment
							+ (!TextUtils.isEmpty(_where) ? " and (" + _where
									+ ")" : ""), _whereArgs);
		default:
			throw new IllegalArgumentException("unsupported URI " + _uri);
		}
		
		getContext().getContentResolver().notifyChange(_uri, null);
		return count;
	}

	@Override
	public String getType(Uri _uri) {
		Log.d(TAG, "getType");
		switch (uriMatcher.match(_uri)) {
		case PREDICTED_TRAINS: return "vnd.android.cursor.dir/vnd.metro.us.dc.predictedtrains";
		case PREDICTED_TRAIN_ID: return "vnd.android.cursor.item/vnd.metro.us.dc.predictedtrains";
		default: throw new IllegalArgumentException("Unsupported URI " + _uri);
		}
	}

	@Override
	public Uri insert(Uri _uri, ContentValues _initialValues) {
		Log.d(TAG, "insert");
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		long rowID = database.insert(USDCTrain.TABLE_USDCTRAINS, "train",
				_initialValues);
		if(rowID>0){
			Uri uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
			getContext().getContentResolver().notifyChange(uri, null);
			return uri;
		}
		throw new SQLException("Failed to insert row into " + _uri);
	}

	@Override
	public boolean onCreate() {
		Log.d(TAG, "onCreate");
		Context context = getContext();
		dbHelper = new MetroDBHelper(context);
		return true;
	}

	@Override
	public Cursor query(Uri _uri, String[] _projection, String _where, String[] _whereArgs,
			String _sort) {
		Log.d(TAG, "query");
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(USDCTrain.TABLE_USDCTRAINS);
		
		switch (uriMatcher.match(_uri)) {
		case PREDICTED_TRAIN_ID: queryBuilder.appendWhere(USDCTrain.COLUMN_ID + "=" + _uri.getPathSegments().get(1));
			break;
		default:
			break;
		}
		
		Cursor c = queryBuilder.query(database, _projection,
				_where, _whereArgs, null, null, _sort);
		
		// notify contexts content resolver
		c.setNotificationUri(getContext().getContentResolver(), _uri);
		return c;
	}

	@Override
	public int update(Uri _uri, ContentValues _values, String _where, String[] _whereArgs) {
		Log.d(TAG, "update");
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		int count;
		
		switch (uriMatcher.match(_uri)) {
		case PREDICTED_TRAINS:
			count = db.update(USDCTrain.TABLE_USDCTRAINS, _values, _where, _whereArgs);
			break;
		case PREDICTED_TRAIN_ID:
			String segment = _uri.getPathSegments().get(1);
			count = db.update(USDCTrain.TABLE_USDCTRAINS, _values,
					USDCTrain.COLUMN_ID
							+ "="
							+ segment
							+ (!TextUtils.isEmpty(_where) ? " and " + " ("
									+ _where + ") " : ""), _whereArgs);
			break;
		default: throw new IllegalArgumentException("Failed to update uri " + _uri);
		}
		
		getContext().getContentResolver().notifyChange(_uri, null);
		return count;
	}
	
	/*private static class MetroDBHelper extends SQLiteOpenHelper{

		private static final String SUBTAG = "ProviderPredictedTrains.MetroDBHelper";
		
		private static final String DATABASE_NAME = "metro.db";
		private static final int DATABASE_VERSION = 1;
		private static final String TABLE_PREDICTED_TRAINS = "predictedtrains";
		
		private static final String DATABASE_CREATE = "create table " + TABLE_PREDICTED_TRAINS + " ("
				+ USDCTrain.COLUMN_ID + " integer primary key autoincrement, " 
				+ USDCTrain.COLUMN_SOURCE_STATION + " text, "
				+ USDCTrain.COLUMN_DESTINATION_STATION + " text, "
				+ USDCTrain.COLUMN_MINS + " text, "
				+ USDCTrain.COLUMN_CARS + " text, "
				+ USDCTrain.COLUMN_LINE + " text);";
		
		// underlying database
		private SQLiteDatabase metroDB;
		
		public MetroDBHelper(Context _context, String _name,
				CursorFactory _factory, int _version) {
			super(_context, _name, _factory, _version);
			Log.d(SUBTAG, "MetroDBHelper");
		}
		
		@Override
		public void onCreate(SQLiteDatabase _db) {
			Log.d(SUBTAG, "onCreate");
			_db.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
			Log.d(SUBTAG, "onUpgrade");
			Log.w(SUBTAG, "Upgrading  DB from " + _oldVersion + " to "
					+ _newVersion + " will destroy all old data");
			_db.execSQL("DROP TABLE IF EXISTS " + TABLE_PREDICTED_TRAINS);
			onCreate(_db);
		}
	}*/
}
