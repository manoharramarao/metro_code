package com.metrotransit.us.dc.vo;

import com.google.gson.annotations.SerializedName;

public class Stop {
	
	@SerializedName("StopID")
	String stopId;
	
	@SerializedName("Lat")
	String lat;
	
	@SerializedName("Lon")
	String lng;
	
	@SerializedName("Name")
	String name;
	
	@SerializedName("Routes")
	String[] routes;
	
	public String getStopId() {
		return stopId;
	}
	public void setStopId(String stopId) {
		this.stopId = stopId;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String[] getRoutes() {
		return routes;
	}
	public void setRoutes(String[] routes) {
		this.routes = routes;
	}
	
	
}
