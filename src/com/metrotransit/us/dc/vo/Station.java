package com.metrotransit.us.dc.vo;

import com.google.gson.annotations.SerializedName;

public class Station {
	
	@SerializedName("Code")
	String code;
	
	@SerializedName("Lat")
	double lat;
	
	@SerializedName("LineCode1")
	String lineCode1;
	
	@SerializedName("LineCode2")
	String lineCode2;
	
	@SerializedName("LineCode3")
	String lineCode3;
	
	@SerializedName("LineCode4")
	String lineCode4;
	
	@SerializedName("Lon")
	double lon;
	
	@SerializedName("Name")
	String name;
	
	@SerializedName("StationTogether1")
	String stationTogether1;
	
	@SerializedName("StationTogether2")
	String stationTogether2;
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public String getLineCode1() {
		return lineCode1;
	}
	public void setLineCode1(String lineCode1) {
		this.lineCode1 = lineCode1;
	}
	public String getLineCode2() {
		return lineCode2;
	}
	public void setLineCode2(String lineCode2) {
		this.lineCode2 = lineCode2;
	}
	public String getLineCode3() {
		return lineCode3;
	}
	public void setLineCode3(String lineCode3) {
		this.lineCode3 = lineCode3;
	}
	public String getLineCode4() {
		return lineCode4;
	}
	public void setLineCode4(String lineCode4) {
		this.lineCode4 = lineCode4;
	}
	public double getLon() {
		return lon;
	}
	public void setLon(double lon) {
		this.lon = lon;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStationTogether1() {
		return stationTogether1;
	}
	public void setStationTogether1(String stationTogether1) {
		this.stationTogether1 = stationTogether1;
	}
	public String getStationTogether2() {
		return stationTogether2;
	}
	public void setStationTogether2(String stationTogether2) {
		this.stationTogether2 = stationTogether2;
	}
	
	
}
