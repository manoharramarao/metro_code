package com.metrotransit.us.dc.core;

import java.util.HashMap;


public class DCMetroConstants {
		// TODO move it to MetroApplication class which is available across application
		public static final USDCMetroClient dcMetroClient = new USDCMetroClientImpl();
		
		public static final String BRD = "BRD";
		public static final String MINS = "mins";
		public static final String ARR = "arr";
		// TODO all stations are in strings.xml as well. Try to reuse.
		// TODO move it to MetroApplication class
		public static final HashMap<String, String[]> locationNameCode = new HashMap<String, String[]>(){
			{
				put("Dupont Circle", new String[]{"A03"});
				put("Farragut North", new String[]{"A02"});
				put("Metro Center", new String[] {"C01","A01"});
				put("Tenleytown", new String[] {"A07"});
				put("Van Ness UDC", new String[] {"A06"});
				put("Cleveland Park", new String[] {"A05"});
				put("Woodley Park Zoo", new String[] {"A04"});
				put("Judiciary Square", new String[] {"B02"});
				put("National Arpt", new String[] {"C10"});
				put("Greenbelt", new String[] {"E10"});
				put("Stadium Armory", new String[] {"D08"});
				put("Silver Spring", new String[] {"B08"});
				put("Forest Glen", new String[] {"B09"});
				put("Minnesota Avenue", new String[] {"D09"});
				put("Eastern Market", new String[] {"D06"});
				put("Potomac Avenue", new String[] {"D07"});
				put("Federal Center SW", new String[] {"D04"});
				put("Capitol South", new String[] {"D05"});
				put("Smithsonian", new String[] {"D02"});
				put("L'Enfant Plaza", new String[] {"D03","F03"});
				put("Gallery Place", new String[] {"B01","F01"});
				put("Federal Triangle", new String[] {"D01"});
				put("Union Station", new String[] {"B03"});
				put("Rhode Island Avenue", new String[] {"B04"});
				put("Brookland", new String[] {"B05"});
				put("Fort Totten", new String[] {"B06","E06"});
				put("Takoma", new String[] {"B07"});
				put("Virginia Square", new String[] {"K03"});
				put("E Falls Church", new String[] {"K05"});
				put("Ballston", new String[] {"K04"});
				put("Branch Avenue", new String[] {"F11"});
				put("Suitland", new String[] {"F10"});
				put("Landover", new String[] {"D12"});
				put("Largo Town Center", new String[] {"G05"});
				put("Morgan Blvd", new String[] {"G04"});
				put("Court House", new String[] {"K01"});
				put("Benning Road", new String[] {"G01"});
				put("Clarendon", new String[] {"K02"});
				put("Addison Road", new String[] {"G03"});
				put("Capitol Heights", new String[] {"G02"});
				put("Dunn Loring", new String[] {"K07"});
				put("W Falls Church", new String[] {"K06"});
				put("Vienna", new String[] {"K08"});
				put("Twinbrook", new String[] {"A13"});
				put("White Flint", new String[] {"A12"});
				put("Grosvenor", new String[] {"A11"});
				put("Medical Center", new String[] {"A10"});
				put("Huntington", new String[] {"C15"});
				put("Eisenhower Avenue", new String[] {"C14"});
				put("Shady Grove", new String[] {"A15"});
				put("New York Avenue", new String[] {"B35"});
				put("Pentagon", new String[] {"C07"});
				put("Crystal City", new String[] {"C09"});
				put("Pentagon City", new String[] {"C08"});
				put("Prince Georges Plaza", new String[] {"E08"});
				put("West Hyattsville", new String[] {"E07"});
				put("Georgia Avenue", new String[] {"E05"});
				put("Columbia Heights", new String[] {"E04"});
				put("U Street", new String[] {"E03"});
				put("Mt Vernon Sq", new String[] {"E01"});
				put("Farragut West", new String[] {"C03"});
				put("McPherson Square", new String[] {"C02"});
				put("Rosslyn", new String[] {"C05"});
				put("Foggy Bottom", new String[] {"C04"});
				put("Shaw", new String[] {"E02"});
				put("Arlington Cemetery", new String[] {"C06"});
				put("College Park", new String[] {"E09"});
				put("Franconia-Springf'ld", new String[] {"J03"});
				put("King Street", new String[] {"C13"});
				put("Braddock Road", new String[] {"C12"});
				put("Southern Ave", new String[] {"F08"});
				put("Naylor Road", new String[] {"F09"});
				put("Waterfront", new String[] {"F04"});
				put("Navy Yard", new String[] {"F05"});
				put("Anacostia", new String[] {"F06"});
				put("Congress Height", new String[] {"F07"});
				put("Van Dorn St", new String[] {"J02"});
				put("Archives", new String[] {"F02"});
				put("Wheaton", new String[] {"B10"});
				put("Glenmont", new String[] {"B11"});
				put("Bethesda", new String[] {"A09"});
				put("Friendship Heights", new String[] {"A08"});
				put("New Carrollton", new String[] {"D13"});
				put("Deanwood", new String[] {"D10"});
				put("Cheverly", new String[] {"Cheverly"});
			}
		};
		
}
