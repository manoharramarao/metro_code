package com.metrotransit.us.dc.common;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.metrotransit.us.dc.tables.USDCStop;
import com.metrotransit.us.dc.tables.USDCTrain;
import com.metrotransit.us.dc.tables.WidgetDetails;

/**
 * I help in doing DB operations. List of operations
 * <li>creating DB</li>
 * <li>copy DB</li>
 * <li>open DB</li>
 * <li>Tell if DB already exist</li>
 * <li>close DB</li>
 * <li>what needs to be done on creating DB</li>
 * <li>what needs to be done on upgrading DB on different releases</li>
 * 
 * @author manohar
 *
 */
public class MetroDBHelper extends SQLiteOpenHelper{

	private static final String TAG = "MetroDBHelper";
	public static final int DB_VERSION = 1;
	public static final String DB_PATH = "/data/data/com.metrotransit/databases/";
	public static final String DB_NAME = "metro.db";
	
	private SQLiteDatabase database;
	private final Context context;
	
	public MetroDBHelper(Context _context){
		super(_context, DB_NAME, null, DB_VERSION);
		Log.d(TAG, "MetroDBHelper");
		this.context = _context;
	}
	
	public void createDB(){
		Log.d(TAG, "createDB");
		if(dbExist()){
			Log.d(TAG, "DB already exists");
		}else{
			this.getReadableDatabase();
			this.close();
			try{
				copyDB();
				Log.d(TAG, "copyDB went smooth");
			}catch(IOException ioe){
				throw new Error("Error copying database");
			}finally{
				this.close();
			}
		}
	}
	
	private boolean dbExist(){
		Log.d(TAG, "dbExist");
		SQLiteDatabase dbToCheck = null;
		try{
			String path = DB_PATH + DB_NAME;
			dbToCheck = SQLiteDatabase.openDatabase(path, null,SQLiteDatabase.OPEN_READONLY);
			/*dbToCheck = SQLiteDatabase.openDatabase(path, null,SQLiteDatabase.NO_LOCALIZED_COLLATORS);*/
		}catch(SQLiteException sqle){
			// db doesn't exist
		}finally{
			if(dbToCheck != null){
				dbToCheck.close();
			}
		}
		return (dbToCheck!=null)? true: false;
	}
	
	private void copyDB() throws IOException{
		Log.d(TAG, "copyDB");
		// open local db as input stream
		InputStream is = context.getAssets().open(DB_NAME);
		
		// path to create DB file
		String outFileName = DB_PATH + DB_NAME;
		
		// open empty db in output stream
		OutputStream os = new FileOutputStream(outFileName);
    	
		// transfer bytes from is to os
		byte[] buffer = new byte[1024];
		int length;

		while((length = is.read(buffer)) > 0){
			os.write(buffer, 0, length);
		}
		
		// close all streams
		os.flush();
		os.close();
		is.close();
	}
	
	public void openDB() throws SQLException{
		Log.d(TAG, "openDB");
		// open db
		String path = DB_PATH + DB_NAME;
    	database = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READONLY);
		/*database = SQLiteDatabase.openDatabase(path, null,SQLiteDatabase.NO_LOCALIZED_COLLATORS);*/
	}
	
	@Override
	public synchronized void close(){
		Log.d(TAG, "close");
		if(database != null){
			database.close();
		}
		super.close();
	}
	
	@Override
	public void onCreate(SQLiteDatabase _db) {
		Log.d(TAG, "onCreate");
		/*createDB();*/
		/*USDCStop.onCreate(_db);
		USDCTrain.onCreate(_db);
		WidgetDetails.onCreate(_db);*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
		Log.d(TAG, "onUpgrade");
		Log.w(TAG, "upgrading from " + _oldVersion + " to " + _newVersion);
		/*USDCStop.onUpgrade(_db, _oldVersion, _newVersion);
		USDCRoute.onUpgrade(_db, _oldVersion, _newVersion);*/
	}
}
